﻿// Copyright (c) Mike O'Connor, University Of Bristol. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Linq;
using Nano;
using Nano.Science;
using Narupa.Trajectory;
using Narupa.Trajectory.Load;
using NUnit.Framework;
using SlimMath;
using TestHelper = Narupa.MD.Tests.TestHelper;

namespace Narupa.TrajectoryTests
{
    [TestFixture]
    public class Tests
    {

        private TestHelper helper; 
        [SetUp]
        public void TestSetup()
        {
            helper = new TestHelper();
        }
        
        [Test]
        public void TestLoadPdbTrajectory()
        {
            string path = Helper.ResolvePath("^/Assets/Trajectories/Tests/testTraj.pdb");
            var traj = Loader.LoadTrajectory(path, helper.Reporter);
            var trajectory = traj as Trajectory.Trajectory;
            if(trajectory == null)
                Assert.Fail();
            Assert.AreEqual(2, trajectory.NumberOfAtoms);
            Assert.AreEqual(3, traj.NumberOfFrames);

            var top = trajectory.Topology as Topology;
            if (top == null)
                Assert.Fail();
            Assert.AreEqual(1, top.NumberOfChains );
            Assert.AreEqual(2, top.NumberOfResidues);

            Assert.AreEqual(2, top.Elements.Count);
            foreach (var element in top.Elements)
            {
                Assert.AreEqual(Element.Hydrogen, element);
            }

            Vector3 atom0 = new Vector3(.1f, 0f, 0f);
            Vector3 atom1 = new Vector3(.2f, 0f, 0f);
            for(int i=0; i< traj.NumberOfFrames; i++)
            {
                Assert.AreEqual(2, traj[i].Positions.Count());
                Assert.AreEqual(atom0, traj[i].Positions[0]);
                Assert.AreEqual(atom1, traj[i].Positions[1]);
                atom0 += new Vector3(.1f, 0,0);
                atom1 += new Vector3(.1f,0,0);
               
            }
        }

        [Test]
        public void TestLoadPdbTrajectoryWithConectRecords()
        {
            string path = Helper.ResolvePath("^/Assets/Trajectories/Tests/benzamidine.pdb");
            var traj = Loader.LoadTrajectory(path, helper.Reporter);
            var trajectory = traj as Trajectory.Trajectory;
            if(trajectory == null)
                Assert.Fail();
            Assert.AreEqual(18, trajectory.NumberOfAtoms);
            Assert.AreEqual(1, traj.NumberOfFrames);

            var top = trajectory.Topology as Topology;
            if (top == null)
                Assert.Fail();
            Assert.AreEqual(1, top.NumberOfChains );
            Assert.AreEqual(1, top.NumberOfResidues);

            HashSet<BondPair> bonds = new HashSet<BondPair>();
            bonds.Add(new BondPair(0, 1));
            bonds.Add(new BondPair(0, 2));
            bonds.Add(new BondPair(0, 3));
            bonds.Add(new BondPair(3, 4));
            bonds.Add(new BondPair(3, 7));
            bonds.Add(new BondPair(4, 5));
            bonds.Add(new BondPair(4, 6));
            bonds.Add(new BondPair(7, 8));
            bonds.Add(new BondPair(7, 16));
            bonds.Add(new BondPair(8, 9));
            bonds.Add(new BondPair(8, 10));
            bonds.Add(new BondPair(10, 11));
            bonds.Add(new BondPair(10, 12));
            bonds.Add(new BondPair(12, 13));
            bonds.Add(new BondPair(12, 14));
            bonds.Add(new BondPair(14, 15));
            bonds.Add(new BondPair(14, 16));
            bonds.Add(new BondPair(16, 17));

            HashSet<BondPair> topBonds = new HashSet<BondPair>();
            foreach (var bond in top.Bonds)
            {
                topBonds.Add(bond);
            }
            Assert.AreEqual(bonds.Count, topBonds.Count);
            //Removes all shared bonds
            bonds.ExceptWith(topBonds);
            //Result should be zero, implying the sets are identical.
            Assert.IsTrue(bonds.Count ==0);
            
        }
        
        [Test]
        public void TestLoadXyzTrajectory()
        {
            string path = Helper.ResolvePath("^/Assets/Trajectories/Tests/testTraj.xyz");
            var traj = Loader.LoadTrajectory(path, helper.Reporter);
            Trajectory.Trajectory trajectory = traj as Trajectory.Trajectory;
            if(trajectory == null)
                Assert.Fail();
            Assert.AreEqual(2, trajectory.NumberOfAtoms);
            Assert.AreEqual(3, traj.NumberOfFrames);

            var top = trajectory.Topology as Topology;
            if (top == null)
                Assert.Fail();
            Assert.AreEqual(1, top.NumberOfChains );
            Assert.AreEqual(1, top.NumberOfResidues);
            
            Assert.AreEqual(1, top.Bonds.Count());
            Assert.AreEqual(2, top.Elements.Count);
            foreach (var element in top.Elements)
            {
                Assert.AreEqual(Element.Hydrogen, element);
            }

            Vector3 atom0 = new Vector3(.1f, 0f, 0f);
            Vector3 atom1 = new Vector3(.2f, 0f, 0f);
            for(int i=0; i< traj.NumberOfFrames; i++)
            {
                Assert.AreEqual(2, traj[i].Positions.Count());
                Assert.AreEqual(atom0, traj[i].Positions[0]);
                Assert.AreEqual(atom1, traj[i].Positions[1]);
                atom0 += new Vector3(.1f, 0,0);
                atom1 += new Vector3(.1f,0,0);
               
            }
        }

        [Test]
        public void TestLoadXyzTrajectoryWithTopology()
        {
            string path = Helper.ResolvePath("^/Assets/Trajectories/Tests/testTraj.pdb");
            string trajPath = Helper.ResolvePath("^/Assets/Trajectories/Tests/testTraj.xyz");
            var traj = Loader.LoadTrajectory(trajPath, helper.Reporter, topologyPath:path);
            Trajectory.Trajectory trajectory = traj as Trajectory.Trajectory;
            if(trajectory == null)
                Assert.Fail();
            Assert.AreEqual(2, trajectory.NumberOfAtoms);
            Assert.AreEqual(3, traj.NumberOfFrames);

            var top = trajectory.Topology as Topology;
            if (top == null)
                Assert.Fail();
            Assert.AreEqual(1, top.NumberOfChains );
            Assert.AreEqual(2, top.NumberOfResidues);

            foreach (var element in top.Elements)
            {
                Assert.AreEqual(Element.Hydrogen, element);
            }

            Vector3 atom0 = new Vector3(.1f, 0f, 0f);
            Vector3 atom1 = new Vector3(.2f, 0f, 0f);
            for(int i=0; i< traj.NumberOfFrames; i++)
            {
                Assert.AreEqual(2, traj[i].Positions.Count());
                Assert.AreEqual(atom0, traj[i].Positions[0]);
                Assert.AreEqual(atom1, traj[i].Positions[1]);
                atom0 += new Vector3(.1f, 0,0);
                atom1 += new Vector3(.1f,0,0);
               
            }
        }
        
        [Test]
        public void TestLoadXyzBonds()
        {
            string trajPath = Helper.ResolvePath("^/Assets/Trajectories/Tests/pyridine.xyz");
            var traj = Loader.LoadTrajectory(trajPath, helper.Reporter);
            Trajectory.Trajectory trajectory = traj as Trajectory.Trajectory;
            if(trajectory == null)
                Assert.Fail();
            Assert.AreEqual(11, trajectory.NumberOfAtoms);
            Assert.AreEqual(1, traj.NumberOfFrames);

            var top = trajectory.Topology as Topology;
            if (top == null)
                Assert.Fail();
            Assert.AreEqual(1, top.NumberOfChains );
            Assert.AreEqual(1, top.NumberOfResidues);
            Assert.AreEqual(11, top.Bonds.Count());
        }

        [Test]
        public void TestLoadDcdTrajectory()
        {
            
        }
        
        [Test]
        public void TestLoadTrajectoryInvalidExtension()
        {
            string trajPath = "nonsense.stuff";
            try
            {
                Loader.LoadTrajectory(trajPath, helper.Reporter);
            }
            catch (NotSupportedException)
            {
            }
        }
        
        [Test]
        public void TestLoadTrajectoryNoTopology()
        {
            string trajPath = "nonsense.dcd";
           
            try
            {
                Loader.LoadTrajectory(trajPath, helper.Reporter);
            }
            catch (NotSupportedException)
            {
            }
        }

        [Test]
        public void TestLoadTrajectoryNoExtension()
        {
            string trajPath = "nonsense";
           
            try
            {
                Loader.LoadTrajectory(trajPath, helper.Reporter);
            }
            catch (NotSupportedException)
            {
            }   
        }
    }
}