﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using System.Xml;
using Nano;
using Nano.Loading;
using Nano.Science.Simulation;
using Nano.Science.Simulation.Instantiated;
using Nano.Transport.Agents;
using Nano.Transport.Comms;
using Nano.Transport.Variables;
using Narupa.MD.System;
using Narupa.VMD.IMD.API;

namespace Narupa.VMD.IMD
{
    [XmlName("IMDSimulation")]
    public class ImdSimulation : SimulationBase
    {
        private ImdAtomicSystem activeSystem;
        private float averageComputeTime;
        private IVariable<float> computationTimeIVar;
        private float frameComputeTime;
        private bool iVarsInitialised;
        private bool runForm = false;
        private float simulationTime;
        private long timingSteps;
        private Topology topology;
        private TransportContext transportContext;
        private ImdSettings imdSettings;
        private InstantiatedTopology activeTopology;
        private ImdManager imdManager;
        private Stopwatch clock = new Stopwatch();
        private bool paused;
        private int step;

        public ImdSimulation(IReporter reporter) : base(reporter)
        {
        }

        public override string Name { get; set; }

        public override IAtomicSystem ActiveSystem
        {
            get { return activeSystem; }
        }

        public override InstantiatedTopology ActiveTopology
        {
            get { return activeTopology; }
            set { activeTopology = value; }
        }

        public override string Address => "/imdSim";

        public override bool HasReset { get; set; }

        public override bool VariablesInitialised
        {
            get { return iVarsInitialised; }
        }

        public override float SimulationTime
        {
            get { return simulationTime; }
        }

        public override void AttachTransportContext(TransportContext transportContext)
        {
            this.transportContext = transportContext;
        }

        public override void DetachTransportContext(TransportContext transportContext)
        {
        }

        public override void InitialiseVariables(VariableManager variableManager)
        {
            computationTimeIVar = variableManager.Variables.Add(VariableName.ComputationTime, VariableType.Float, true) as IVariable<float>;

            ((IVariableProvider)ActiveSystem).InitialiseVariables(variableManager);
            ((IVariableProvider)ActiveSystem).ResetVariableDesiredValues();
            ((IVariableProvider)ActiveSystem).UpdateVariableValues();
        }

        public override void Load(LoadContext context, XmlNode node)
        {
            Name = Helper.GetAttributeValue(node, nameof(Name), "ImdSimulation");
            imdSettings = Loader.LoadObject<ImdSettings>(context, node);
            topology = Loader.LoadObject<Topology>(context, node);

            InitialiseSimulation();
        }

        public override void Reset()
        {
            DetachTransportContext(transportContext);
            ActiveSystem.Dispose();
            imdManager.Dispose();

            InitialiseSimulation();

            AttachTransportContext(transportContext);
            ActiveTopology.AttachTransportContext(transportContext);

            simulationTime = 0f;

            HasReset = true;
        }

        public override void ResetVariableDesiredValues()
        {
            ((IVariableProvider)ActiveSystem).ResetVariableDesiredValues();
        }

        public override void Run(int steps, List<Interaction> interactionPoints = null)
        {
            if (paused)
            {
                imdManager.SendPlay();
                paused = false;
            }
            clock.Restart();
            frameComputeTime = (float)clock.ElapsedTicks / Stopwatch.Frequency * 1000;
            activeSystem.Interactions.SetInteractions(interactionPoints);
            activeSystem.CalculateForceFields();
            imdManager.Update(activeSystem);
            clock.Stop();
			step++;
            activeSystem.SimulationLogManager.Log(activeSystem, (long)imdManager.SimTimeStep, 1);
        }

        public override XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "RunForm", runForm);
            element.AppendChild(Loader.SaveObject(context, element, topology));
            return element;
        }

        public override void UpdateVariableValues()
        {
            averageComputeTime = (frameComputeTime + timingSteps * averageComputeTime) / (timingSteps + 1);
            timingSteps++;
            if (timingSteps > 10000)
            {
                Reporter.PrintDetail($"Average compute time for last 10000 steps: ${averageComputeTime}");
                timingSteps = 0;
                averageComputeTime = 0f;
            }
            computationTimeIVar.Value = averageComputeTime;
            ((IVariableProvider)ActiveSystem).UpdateVariableValues();
        }

        private void InitialiseSimulation()
        {
            activeTopology = new InstantiatedTopology();
            //TODO this is messy. Read from somewhere?
            SimulationBoundary boundary = new SimulationBoundary();
            boundary.SetDefaults();
            var ibound = boundary as ISimulationBoundary;
            topology.Instantiate(activeTopology, ref ibound, Reporter);

            activeTopology.GenerateMolecules();

            try
            {
                activeSystem = new ImdAtomicSystem(ActiveTopology, boundary, Reporter, imdSettings.Loggers);
                Reporter?.PrintEmphasized("Created a Plumed system of {0} particles.", ActiveSystem.NumberOfParticles);
            }
            catch (Exception e)
            {
                Reporter?.PrintException(e, "Exception thrown when attemping to create active atomic system.");
                throw;
            };

            if (imdManager != null)
            {
                imdManager.Dispose();
            }
            imdManager = new ImdManager(imdSettings, Reporter);
            
            if (imdSettings.Command != null)
            {
                if (imdSettings.Command.IsRunning == false) imdSettings.Command.Start(Reporter, imdSettings.Port);
                else
                {
                    if (imdSettings.Command.SupportsPortArgument)
                    {
                        imdSettings.Port++;
                    }
                    imdManager.SendKill();
                    //Give the process a chance to kill itself.
                    Thread.Sleep(1000);
                    imdSettings.Command.Restart(imdSettings.Port);
                }
                Thread.Sleep(5000);
            }
            
            try
            {
                imdManager.Connect();
            }
            catch (Exception e)
            {
                Reporter?.PrintWarning(ReportVerbosity.Normal, "Failed to connect to IMD simulation, will keep trying...");
            }
        }

        public override void Pause()
        {
            if (!paused)
            {
                imdManager?.SendPause();
                paused = true;
            }
        }

        public override bool EndOfSimulation => false;
        public override void Dispose()
        {
            activeSystem?.Dispose();
            imdManager?.Dispose();
        }
        
    }
}