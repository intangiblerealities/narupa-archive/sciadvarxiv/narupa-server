﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Xml;
using Nano;
using Nano.Loading;
using Nano.Server;
using Nano.Server.Basic;
using Rug.Cmd;

namespace Narupa.Server
{
    [XmlName("Server")]
    public class Server : IServer
    {
        private const string configFile = "^/server.xml";

        private readonly StringArgument nameArgument = new StringArgument("name", "Server name", "Server name");
        
        public Server(string description)
        {
            Description = description;
        }

        public void Load(LoadContext context, XmlNode node)
        {
            Name = Helper.GetAttributeValue(node, nameof(Name), Description);

            Discovery = Loader.LoadObject<IDiscovery>(context, nameof(Discovery), node) ?? new AhoyDiscovery();
            Monitor = Loader.LoadObject<IMonitor>(context, nameof(Monitor), node) ?? new DumyMonitor();
            //@review @ptew i had to explicitly add the type here or it threw a null reference exception because it could not find the constructor for Nano.Server.Basic.ReportManager.
            ReportManager = Loader.LoadObject<IReportManager>(context, nameof(ReportManager), node, new Type[] { typeof(ReportVerbosity) }, new object[]{ ReportVerbosity.Normal}  ) ?? new ReportManager();
            AssetManager = Loader.LoadObject<IAssetManager>(context, nameof(AssetManager), node) ?? new FileSystemAssetManager();
            SecurityProvider = Loader.LoadObject<ISecurityProvider>(context, nameof(SecurityProvider), node) ?? new OpenSecurityProvider();
            Service = Loader.LoadObject<IService>(context, nameof(Service), node) ?? throw new Exception("No service defined");
        }

        public XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, nameof(Name), Name);

            Loader.SaveObject(context, element, Discovery, nameof(Discovery));
            Loader.SaveObject(context, element, Monitor, nameof(Monitor));
            Loader.SaveObject(context, element, ReportManager, nameof(ReportManager));
            Loader.SaveObject(context, element, AssetManager, nameof(AssetManager));
            Loader.SaveObject(context, element, SecurityProvider, nameof(SecurityProvider));
            Loader.SaveObject(context, element, Service, nameof(Service));

            return element;
        }

        /// <inheritdoc />
        public IAssetManager AssetManager { get; private set; }

        /// <inheritdoc />
        public IDiscovery Discovery { get; private set; }

        /// <inheritdoc />
        public IMonitor Monitor { get; private set; }

        /// <inheritdoc />
        public string Name { get; private set; }

        /// <inheritdoc />
        public IReportManager ReportManager { get; private set; }

        /// <inheritdoc />
        public IService Service { get; private set; }

        /// <inheritdoc />
        public ISecurityProvider SecurityProvider { get; private set; } 

        /// <inheritdoc />
        public string Description { get; }

        public void Load(IReporter reporter, string configFile = configFile)
        {
            XmlDocument xmlDocument = new XmlDocument();

            xmlDocument.Load(Helper.ResolvePath(configFile));

            LoadContext loadContext = new LoadContext(reporter);

            Load(loadContext, xmlDocument.DocumentElement);

            if (loadContext.Errors.Count > 0)
            {
                throw new Exception($"{loadContext.Errors.Count} errors while loading server config file \"{configFile}\".");
            }
        }

        public void RegisterArguments(ArgumentParser parser)
        {
            nameArgument.Reset(); 
            parser.Add("-", "Name", nameArgument); 
            
            ReportManager?.RegisterArguments(parser);
            Discovery?.RegisterArguments(parser);
            AssetManager?.RegisterArguments(parser);
            Monitor?.RegisterArguments(parser);
            SecurityProvider?.RegisterArguments(parser);
            Service?.RegisterArguments(parser);
        }

        public void Save(IReporter reporter, string configFile = configFile)
        {
            XmlDocument xmlDocument = new XmlDocument();

            LoadContext loadContext = new LoadContext(reporter);

            XmlElement node = Helper.CreateElement(xmlDocument, "Server");

            XmlElement simulationNode = Loader.SaveObject(loadContext, node, this);

            xmlDocument.AppendChild(simulationNode);

            if (loadContext.Errors.Count > 0)
            {
                throw new Exception($"{loadContext.Errors.Count} errors while saving server config file \"{configFile}\".");
            }

            xmlDocument.Save(Helper.ResolvePath(configFile));
        }

        public void ValidateArguments(IReporter reporter)
        {                
            if (nameArgument.Defined == true)
            {
                Name = nameArgument.Value; 
            }
            
            ReportManager?.ValidateArguments(reporter);
            Discovery?.ValidateArguments(reporter);
            AssetManager?.ValidateArguments(reporter);
            Monitor?.ValidateArguments(reporter);
            SecurityProvider?.ValidateArguments(reporter);
            Service?.ValidateArguments(reporter);
        }

        /// <inheritdoc />
        public void Dispose()
        {
            (ReportManager as IDisposable)?.Dispose(); 
            (Discovery as IDisposable)?.Dispose(); 
            (AssetManager as IDisposable)?.Dispose(); 
            (Monitor as IDisposable)?.Dispose(); 
            (SecurityProvider as IDisposable)?.Dispose(); 
            (Service as IDisposable)?.Dispose(); 
        }
    }
}