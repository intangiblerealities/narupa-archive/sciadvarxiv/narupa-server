using System.Collections.Generic;
using Nano;
using Nano.Science;
using SlimMath;

namespace Narupa.Trajectory.Load.XYZ
{
    /// <summary>
    /// Methods for loading an XYZ file in as a trajectory.
    /// </summary>
    public static class XyzTrajectory
    {
        /// <summary>
        ///     Loads a trajectory and topology from PDB file.
        /// </summary>
        /// <param name="trajectoryFile">The PDB file from which to load the trajectory.</param>
        /// <param name="reporter">Reporter to output information to.</param>
        /// <param name="reactive">
        ///     Whether this trajectory represents a reactive trajectory, in which the topology can change frame
        ///     to frame.
        /// </param>
        /// <returns></returns>
        public static ITrajectory LoadTrajectory(string trajectoryFile, IReporter reporter, bool reactive = false)
        {
            var file = new XyzFile();
            file.Read(trajectoryFile);

            ITrajectory traj;
            if (reactive == false)
            {
                var frame = file.Frames[0];
                var topology = GenerateTopology(frame, reporter, file.Identifier);
                traj = GenerateTrajectory(file, reporter, topology);
            }
            else
            {
                traj = GenerateReactiveTrajectory(file, reporter);
            }

            return traj;
        }

        private static ITopology GenerateTopology(XyzFrame frame, IReporter reporter, string identifier)
        {
            var top = new Topology();
            var chain = top.AddChain();
            var residue = top.AddResidue(identifier, chain);
            var elements = new List<Element>();
            var positions = new List<Vector3>();
            for (var i = 0; i < frame.NumberOfAtoms; i++)
            {
                var element = Utility.GetElement(frame.AtomNames[i]);
                elements.Add(element);
                positions.Add(frame.AtomPositions[i] * Units.NmPerAngstrom);
                top.AddAtom(frame.AtomNames[i], element, residue);
            }

            var bonds = Utility.GenerateBondsDistance(elements, positions);
            foreach (var bond in bonds) top.AddBond(bond);

            return top;
        }

        /// <summary>
        ///     Loads a trajectory from an XYZ file, given an existing topology.
        /// </summary>
        /// <param name="trajectoryFile">The XYZ file from which to load the trajectory.</param>
        /// <param name="reporter">The reporter to print information to.</param>
        /// <param name="topology">The topoloy to use for the trajectory.</param>
        /// <returns></returns>
        public static ITrajectory LoadTrajectory(string trajectoryFile, IReporter reporter, ITopology topology)
        {
            var file = new XyzFile();
            file.Read(trajectoryFile);

            var traj = GenerateTrajectory(file, reporter, topology);
            return traj;
        }

        private static ITrajectory GenerateReactiveTrajectory(XyzFile xyzFile, IReporter reporter)
        {
            var trajectory = new Trajectory();
            foreach (var xyzFrame in xyzFile.Frames)
            {
                var positions = new List<Vector3>(xyzFrame.AtomPositions.Count);
                var topology = GenerateTopology(xyzFrame, reporter, xyzFile.Identifier);
                foreach (var pos in xyzFrame.AtomPositions)
                {
                    positions.Add(pos * Units.NmPerAngstrom);
                }

                var frame = new Frame(topology, positions);
                trajectory.AddFrame(frame);
            }

            return trajectory;
        }

        private static ITrajectory GenerateTrajectory(XyzFile file, IReporter reporter, ITopology topology)
        {
            var trajectory = new Trajectory();
            foreach (var xyzFrame in file.Frames)
            {
                var positions = new List<Vector3>(xyzFrame.AtomPositions.Count);
                foreach (var pos in xyzFrame.AtomPositions)
                {
                    positions.Add(pos * Units.NmPerAngstrom);
                }
                var frame = new Frame(topology, positions);
                trajectory.AddFrame(frame);
            }

            return trajectory;
        }
    }
}