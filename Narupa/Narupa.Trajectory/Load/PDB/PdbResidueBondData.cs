﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

namespace Narupa.Trajectory.Load.PDB
{
    /// <summary>
    ///     Represents the data in a bond within a residue.
    /// </summary>
    /// <remarks>
    ///     Used with 'residues.xml' to represent bonds within a residue.
    ///     For example From="-CA" and To="C" means a bond from the alpha carbon of the
    ///     previous residue to the current residue.
    /// </remarks>
    internal class PdbResidueBondData
    {
        public string From;
        public string To;

        /// <summary>
        ///     Represents the properties a bond within a residue.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="to"></param>
        public PdbResidueBondData(string from, string to)
        {
            From = from;
            To = to;
        }
    }
}