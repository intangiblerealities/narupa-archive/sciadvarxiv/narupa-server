// Copyright (c) Mike O'Connor, University Of Bristol. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;

namespace Narupa.Trajectory
{
    /// <summary>
    /// Represents a residue in a PDB-style topology.
    /// </summary>
    public class Residue
    {
        /// <summary>
        ///     Chain that is residue belongs to.
        /// </summary>
        public Chain Chain;
        
        /// <summary>
        ///     Name of this residue.
        /// </summary>
        public string Name;
        
        /// <summary>
        ///     ID of this residue.
        /// </summary>
        /// <remarks>
        ///     This value is not necessarily sequential, and comes from a PDB. It will be set to the absolute index
        /// for trajectories not from a PDB file
        /// </remarks>
        public int ResId;
        
        /// <summary>
        ///     Segment ID of this residue.
        /// </summary>
        /// <remarks>
        ///     Optional value, typically from a PDB file.
        /// </remarks>
        public string SegmentId;
        
        private readonly List<Atom> atoms = new List<Atom>();

        /// <summary>
        /// Constructs a residue.
        /// </summary>
        /// <param name="name">Name of the residue.</param>
        /// <param name="chain">Chain this residue belongs to.</param>
        /// <param name="resId">The residue ID.</param>
        /// <param name="segmentId">The segment ID.</param>
        public Residue(string name, Chain chain, int resId, string segmentId)
        {
            Name = name;
            Chain = chain;
            ResId = resId;
            SegmentId = segmentId;
        }

        /// <summary>
        /// The atoms in this residue.
        /// </summary>
        public IEnumerable<Atom> Atoms
        {
            get
            {
                foreach (var atom in atoms) yield return atom;
            }
        }

        internal void AddAtom(Atom atom)
        {
            atoms.Add(atom);
        }
    }
}