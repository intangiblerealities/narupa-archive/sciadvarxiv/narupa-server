// Copyright (c) Mike O'Connor, University Of Bristol. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using SlimMath;

namespace Narupa.Trajectory
{
    /// <summary>
    ///     Represents a frame in a typical molecular dynamics trajectory.
    /// </summary>
    public class Frame : IFrame
    {
        private readonly List<Vector3> positions = new List<Vector3>();

        /// <summary>
        /// Constructs a frame.
        /// </summary>
        /// <param name="topology">The topology of the frame.</param>
        /// <param name="positions">The positions of the frame.</param>
        /// <exception cref="Exception"></exception>
        public Frame(ITopology topology, IList<Vector3> positions)
        {
            Topology = topology;
            if (positions.Count != topology.NumberOfAtoms)
                throw new Exception(
                    "The number of atoms in the topology does not match the number of atoms in the list of positions passed into the construction of the frame.");
            this.positions.Clear();
            this.positions.AddRange(positions);
        }

        /// <inheritdoc />
        public ITopology Topology { get; }

        /// <inheritdoc />
        public IList<Vector3> Positions => positions;

        /// <summary>
        /// Set the positions of the atoms in the frame.
        /// </summary>
        /// <param name="positions">New positions.</param>
        /// <exception cref="Exception">Number of positions passed does not match the number of atoms in the frame.</exception>
        public void SetPositions(IList<Vector3> positions)
        {
            if (positions.Count != this.positions.Count)
                throw new Exception("Number of positions passed does not match number of atoms in frame.");
            this.positions.Clear();
            this.positions.AddRange(positions);
        }
    }
}