﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using SlimMath;

namespace Narupa.MD.ForceField.MM3
{
    /// <summary> Struct for storing atom types in an angle </summary>
    public struct AngleIdx
    {
        #region Public Fields

        /// <summary>
        /// The index i
        /// </summary>
        public readonly int I;

        /// <summary>
        /// The index j.
        /// </summary>
        public readonly int J;

        /// <summary>
        /// The index k.
        /// </summary>
        public readonly int K;

        #endregion Public Fields

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AngleIdx"/> struct.
        /// </summary>
        /// <param name="type1">The type1.</param>
        /// <param name="type2">The type2.</param>
        /// <param name="type3">The type3.</param>
        public AngleIdx(int type1, int type2, int type3) : this()
        {
            I = type1;
            J = type2;
            K = type3;
        }

        #endregion Public Constructors
    }

    /// <summary> Struct for storing atom types in a torsion </summary>
    public struct TorsionIdx
    {
        #region Public Fields

        /// <summary>
        /// The index of atom i.
        /// </summary>
        public readonly int I;

        /// <summary>
        /// The index of atom j.
        /// </summary>
        public readonly int J;

        /// <summary>
        /// The index of atom k
        /// </summary>
        public readonly int K;

        /// <summary>
        /// The index of atom l
        /// </summary>
        public readonly int L;

        #endregion Public Fields

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TorsionIdx"/> struct.
        /// </summary>
        /// <param name="type1">The type1.</param>
        /// <param name="type2">The type2.</param>
        /// <param name="type3">The type3.</param>
        /// <param name="type4">The type4.</param>
        public TorsionIdx(int type1, int type2, int type3, int type4) : this()
        {
            I = type1;
            J = type2;
            K = type3;
            L = type4;
        }

        #endregion Public Constructors
    }

    /// <summary> Struct for MM3 Angles, in SoA form. </summary>
    public class AngleStruct
    {
        #region Public Fields

        /// <summary> Force constant of angle </summary>
        public List<float> FC;

        /// <summary> Index of atom I in angle </summary>
        public List<int> IndexI;

        /// <summary> Index of atom J in angle </summary>
        public List<int> IndexJ;

        /// <summary> Index of atom K in angle </summary>
        public List<int> IndexK;

        /// <summary> Equilibrium angle between I-J-K </summary>
        public List<float> Theta0;

        #endregion Public Fields

        #region Public Properties

        /// <summary>
        /// Gets the number of angles.
        /// </summary>
        /// <value>The count.</value>
        public int Count { get { return IndexI.Count; } }

        #endregion Public Properties

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="AngleStruct"/> class.
        /// </summary>
        public AngleStruct()
        {
            IndexI = new List<int>();
            IndexJ = new List<int>();
            IndexK = new List<int>();
            FC = new List<float>();
            Theta0 = new List<float>();
        }

        #endregion Public Constructors

        internal AngleStruct Clone()
        {
            AngleStruct a = new AngleStruct();
            a.FC = new List<float>(FC);
            a.IndexI = new List<int>(IndexI);
            a.IndexJ = new List<int>(IndexJ);
            a.IndexK = new List<int>(IndexK);
            a.Theta0 = new List<float>(Theta0);
            return a;
        }
    }

    /// <summary> Struct for MM3 Bonds, in SoA form. </summary>
    public class BondStruct
    {
        #region Public Fields

        /// <summary> Force constant of bond </summary>
        public List<float> FC;

        /// <summary> Index of atom I in bond </summary>
        public List<int> IndexI;

        /// <summary> Index of atom J in bond </summary>
        public List<int> IndexJ;

        /// <summary> Equilibrium distance of bond in Angstroms </summary>
        public List<float> R0;

        #endregion Public Fields

        #region Public Properties

        /// <summary>
        /// Gets the number of bonds.
        /// </summary>
        /// <value>The count.</value>
        public int Count { get { return IndexI.Count; } }

        #endregion Public Properties

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="BondStruct"/> class.
        /// </summary>
        public BondStruct()
        {
            IndexI = new List<int>();
            IndexJ = new List<int>();
            FC = new List<float>();
            R0 = new List<float>();
        }

        #endregion Public Constructors

        internal BondStruct Clone()
        {
            BondStruct b = new BondStruct();
            b.FC = new List<float>(FC);
            b.IndexI = new List<int>(IndexI);
            b.IndexJ = new List<int>(IndexJ);
            b.R0 = new List<float>(R0);
            return b;
        }
    }

    /// <summary> Struct for MM3 Morse Potentials, in SoA form. </summary>
    public class MorseStruct
    {
        #region Public Properties

        /// <summary> 'Width' of potential </summary>
        public List<float> Alpha { get; private set; }

        /// <summary> Number of morse potential terms </summary>
        public int Count { get { return IndexI.Count; } }

        /// <summary> Well depth of the bond </summary>
        public List<float> De { get; private set; }

        /// <summary> Index of atom I in bond </summary>
        public List<int> IndexI { get; private set; }

        /// <summary> Index of atom J in bond </summary>
        public List<int> IndexJ { get; private set; }

        /// <summary> Equilibrium distance </summary>
        public List<float> R0 { get; private set; }

        #endregion Public Properties

        #region Public Constructors

        /// <summary> Constructor for new Morse struct. </summary>
        public MorseStruct()
        {
            IndexI = new List<int>();
            IndexJ = new List<int>();
            R0 = new List<float>();
            De = new List<float>();
            Alpha = new List<float>();
        }

        #endregion Public Constructors

        #region Public Methods

        /// <summary> Add a morse term to morse struct </summary>
        public void AddTerm(int i, int j, float r0, float de, float alpha)
        {
            IndexI.Add(i);
            IndexJ.Add(j);
            R0.Add(r0);
            De.Add(de);
            Alpha.Add(alpha);
        }

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns>MorseStruct.</returns>
        internal MorseStruct Clone()
        {
            MorseStruct m = new MorseStruct();
            m.Alpha = new List<float>(Alpha);
            m.De = new List<float>(De);
            m.IndexI = new List<int>(IndexI);
            m.IndexJ = new List<int>(IndexJ);
            m.R0 = new List<float>(R0);

            return m;
        }

        #endregion Public Methods
    }

    /// <summary>
    /// Represents an out of plane bend.
    /// </summary>
    public class OPBend
    {
        /// <summary>
        /// The force constant.
        /// </summary>
        public readonly float ForceConstant;

        /// <summary>
        /// The atom A.
        /// </summary>
        public readonly int AtomA;

        /// <summary>
        /// The atom B.
        /// </summary>
        public readonly int AtomB;

        /// <summary>
        /// The atom C.
        /// </summary>
        public readonly int AtomC;

        /// <summary>
        /// The atom D.
        /// </summary>
        public readonly int AtomD;

        /// <summary>
        /// Initializes a new instance of the <see cref="OPBend"/> class.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <param name="c">The c.</param>
        /// <param name="d">The d.</param>
        /// <param name="forceConstant">The force constant.</param>
        public OPBend(int a, int b, int c, int d, float forceConstant)
        {
            AtomA = a;
            AtomB = b;
            AtomC = c;
            AtomD = d;
            ForceConstant = forceConstant;
        }
    }

    /// <summary>
    /// Represents  Out of Plane Bend terms, in SoA form.
    /// </summary>
    public class OPBendTerms
    {
        #region Public Properties

        /// <summary>
        /// Gets the number of out of plane bends.
        /// </summary>
        /// <value>The count.</value>
        public int Count { get { return IndexA.Count; } }

        /// <summary>
        /// Gets the force constant.
        /// </summary>
        /// <value>The force constant.</value>
        public List<float> ForceConstants { get; private set; }

        /// <summary>
        /// Gets the list of index a.
        /// </summary>
        /// <value>The index a.</value>
        public List<int> IndexA { get; private set; }

        /// <summary>
        /// Gets the index b.
        /// </summary>
        /// <value>The index b.</value>
        public List<int> IndexB
        {
            get; private set;
        }

        /// <summary>
        /// Gets the index c.
        /// </summary>
        /// <value>The index c.</value>
        public List<int> IndexC { get; private set; }

        /// <summary>
        /// Gets the index d.
        /// </summary>
        /// <value>The index d.</value>
        public List<int> IndexD { get; private set; }

        #endregion Public Properties

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="OPBendTerms"/> class.
        /// </summary>
        public OPBendTerms()
        {
            IndexA = new List<int>();
            IndexB = new List<int>();
            IndexC = new List<int>();
            IndexD = new List<int>();
            ForceConstants = new List<float>();
        }

        #endregion Public Constructors

        #region Public Methods

        /// <summary>
        /// Adds the term.
        /// </summary>
        /// <param name="a">a.</param>
        /// <param name="b">The b.</param>
        /// <param name="c">The c.</param>
        /// <param name="d">The d.</param>
        /// <param name="fc">The force constant.</param>
        /// <returns>System.Int32.</returns>
        public int AddTerm(int a, int b, int c, int d, float fc)
        {
            IndexA.Add(a);
            IndexB.Add(b);
            IndexC.Add(c);
            IndexD.Add(d);
            ForceConstants.Add(fc);
            return Count;
        }

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns>OPBendStruct.</returns>
        /// <exception cref="NotImplementedException"></exception>
        internal OPBendTerms Clone()
        {
            OPBendTerms s = new OPBendTerms();
            s.ForceConstants = new List<float>(ForceConstants);
            s.IndexA = new List<int>(IndexA);
            s.IndexB = new List<int>(IndexB);
            s.IndexC = new List<int>(IndexC);
            s.IndexD = new List<int>(IndexD);

            return s;
        }

        internal void AddTerm(OPBend opBend)
        {
            IndexA.Add(opBend.AtomA);
            IndexB.Add(opBend.AtomB);
            IndexC.Add(opBend.AtomC);
            IndexD.Add(opBend.AtomD);
            ForceConstants.Add(opBend.ForceConstant);
        }

        #endregion Public Methods
    }

    /// <summary> Struct for MM3 Torsions in SoA form. </summary>
    public class TorsionStruct
    {
        #region Public Fields

        /// <summary>
        /// List of index i
        /// </summary>
        public List<int> IndexI;

        /// <summary>
        /// List of index j
        /// </summary>
        public List<int> IndexJ;

        /// <summary>
        /// List of index k
        /// </summary>
        public List<int> IndexK;

        /// <summary>
        /// List of index l
        /// </summary>
        public List<int> IndexL;

        /// <summary>
        /// List of torsion vectors.
        /// </summary>
        public List<Vector3> V;

        #endregion Public Fields

        #region Public Properties

        /// <summary>
        /// Gets the number of torsions.
        /// </summary>
        /// <value>The count.</value>
        public int Count { get { return IndexI.Count; } }

        #endregion Public Properties

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="TorsionStruct"/> class.
        /// </summary>
        public TorsionStruct()
        {
            IndexI = new List<int>();
            IndexJ = new List<int>();
            IndexK = new List<int>();
            IndexL = new List<int>();
            V = new List<Vector3>();
        }

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns>TorsionStruct.</returns>
        internal TorsionStruct Clone()
        {
            TorsionStruct t = new TorsionStruct();
            t.IndexI = new List<int>(IndexI);
            t.IndexJ = new List<int>(IndexJ);
            t.IndexK = new List<int>(IndexK);
            t.IndexL = new List<int>(IndexL);
            t.V = new List<Vector3>(V);

            return t;
        }

        #endregion Public Constructors
    }
}