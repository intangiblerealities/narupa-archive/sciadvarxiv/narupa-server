﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using Nano.Transport.OSCCommands;

namespace Narupa.MD.Selection
{
    public partial class AtomSelectionPool : ICommandProvider
    {
        private readonly string[] backboneSelectionStrings = { "//C/*", "//CA/*", "//N/*", "//O/*" };

        private Regex CreateRegexForAtomName(string atomName)
        {
            return new Regex($".*/{atomName}/[0-9]+$");
        }

        private bool IsProtein(AtomSelection selection)
        {
            bool containsC = false;
            bool containsCA = false;
            bool containsN = false;
            bool containsO = false;
            Regex c = CreateRegexForAtomName("C");
            Regex ca = CreateRegexForAtomName("CA");
            Regex n = CreateRegexForAtomName("N");
            Regex o = CreateRegexForAtomName("O");
            foreach (ushort atom in selection)
            {
                string address = topology.Addresses[atom];
                if (c.IsMatch(address)) containsC = true;
                if (ca.IsMatch(address)) containsCA = true;
                if (n.IsMatch(address)) containsN = true;
                if (o.IsMatch(address)) containsO = true;
                if (containsC && containsCA && containsN && containsO)
                    return true;
            }
            return false;
        }

        private bool IsWater(AtomSelection selection)
        {
            Regex o = new Regex(".*/(HOH)|(WAT)");
            foreach (ushort atom in selection)
            {
                string address = topology.Addresses[atom];
                if (o.IsMatch(address) == false) return false;
            }
            return true;
        }

        private AtomSelection CreateBackboneSelection(AtomSelection selection)
        {
            List<AtomSelectionString> backboneAddress = new List<AtomSelectionString>();

            foreach (string pattern in backboneSelectionStrings)
            {
                backboneAddress.Add(new AtomSelectionString(pattern, SelectionType.OscAddress));
            }

            AtomSelection backboneSelection = new AtomSelection(selection.Address, selection.Name + "/backbone", topology.Addresses, false, backboneAddress.ToArray());

            try
            {
                AddSelection(backboneSelection);
                return backboneSelection;
            }
            catch (Exception e)
            {
            }
            return null;
        }
    }
}