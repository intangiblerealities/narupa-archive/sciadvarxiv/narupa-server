﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using Nano;
using Nano.Science.Simulation;
using Nano.Science.Simulation.ForceField;
using Nano.Science.Simulation.Integrator;
using Nano.Transport.Agents;
using Nano.Transport.Variables;
using Narupa.MD.System;
using SlimMath;

namespace Narupa.MD.Dynamics.Integrator
{
    /// <summary>
    /// Represents a Verlet integrator.
    /// </summary>
    /// <seealso cref="IIntegrator" />
    /// <seealso cref="IVariableProvider" />
    public class VerletIntegrator : IIntegrator, IVariableProvider
    {
        #region Public Properties

        bool IVariableProvider.VariablesInitialised
        {
            get
            {
                throw new NotImplementedException();
            }
        }

        #endregion Public Properties

        #region Public Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="VerletIntegrator"/> class.
        /// </summary>
        public VerletIntegrator()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="VerletIntegrator"/> class.
        /// </summary>
        /// <param name="atomicSystem">The atomic system.</param>
        /// <param name="timeStep">The time step.</param>
        /// <param name="removeCmMotion">if set to <c>true</c> the integrator will remove centre of mass motion.</param>
        /// <param name="reporter">The reporter.</param>
        public VerletIntegrator(AtomicSystem atomicSystem, float timeStep, bool removeCmMotion, IReporter reporter)
        {
            system = atomicSystem;
            this.timeStep = timeStep;
            this.reporter = reporter;
            removeCMMotion = removeCmMotion;
            periodicBoxVectors = system.Boundary.GetPeriodicBoxVectors();
        }

        #endregion Public Constructors

        #region Public Methods

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns> IIntegrator.</returns>
        public IIntegrator Clone()
        {
            VerletIntegrator v = new VerletIntegrator();
            v.reporter = reporter;
            v.step = step;
            v.timeStep = timeStep;
            return v;
        }

        /// <summary>
        /// Gets the time step.
        /// </summary>
        /// <returns>System.Single.</returns>
        public float GetTimeStep()
        {
            return timeStep;
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        /// <exception cref="NotImplementedException"></exception>
        void IDisposable.Dispose()
        {
            throw new NotImplementedException();
        }

        /// <summary>
        /// Initialises the specified variable manager.
        /// </summary>
        /// <param name="variableManager">The variable manager.</param>
        void IVariableProvider.InitialiseVariables(VariableManager variableManager)
        {
            timeStepIVar = variableManager.Variables.Add(VariableName.TimeStep, VariableType.Float) as IVariable<float>;

            iVarsInitialised = true;
            (this as IVariableProvider).ResetVariableDesiredValues();
        }

        /// <summary>
        /// Update output IVariables and broadcast values, overriding desired values.
        /// </summary>
        void IVariableProvider.ResetVariableDesiredValues()
        {
            if (!iVarsInitialised) return;
            timeStepIVar.Value = timeStep;
            timeStepIVar.DesiredValue = timeStep;
        }

        /// <summary>
        /// Update input IVariables
        /// </summary>
        void IVariableProvider.UpdateVariableValues()
        {
            if (!iVarsInitialised) return;
            if (timeStepIVar.Readonly == false)
            {
                if (Math.Abs(timeStepIVar.Value - timeStepIVar.DesiredValue) > 0.00001f)
                {
                    timeStep = timeStepIVar.DesiredValue;
                    timeStepIVar.Value = timeStepIVar.DesiredValue;
                }
            }
        }

        /// <summary>
        /// Propagates the specified number of steps.
        /// </summary>
        /// <param name="steps">The steps.</param>
        public void Propagate(int steps)
        {
            for (int i = 0; i < steps; i++)
            {
                RunStep();
            }
        }

        /// <summary>
        /// Resets the integrator.
        /// </summary>
        /// <param name="activeSystem">The active system.</param>
        public void ResetIntegrator(IAtomicSystem activeSystem)
        {
            system = (AtomicSystem)activeSystem;
            step = 0;
        }

        /// <summary>
        /// Propagates the system one step under Velocity Verlet integration.
        /// </summary>
        public void RunStep()
        {
            //Update bounding box
            if (system.Boundary.IsVariableBox)
            {
                system.Boundary.UpdateBoundingBox(system);
                periodicBoxVectors = system.Boundary.GetPeriodicBoxVectors();
            }


            if (step == 0)
            {
                InitialiseInverseMasses();
                InitialiseCandidatePositions();
            }
            //Apply thermostat.

            system.Thermostat?.ApplyStat(system);
            system.Barostat?.ApplyStat(system);

            system.CalculateForceFields();

            //Perform integration
            ApplyForces(system.Particles.Velocity, system.Particles.Force, inverseMasses, timeStep);
            PropagatePositions(system, candidatePositions);

            system.CalculateKineticEnergyAndTemperature();
            
            ApplyConstraints();

            if (system.Boundary.PeriodicBoundary && system.Boundary.Resizing == false)
                IntegratorHelper.WrapMoleculesIntoPeriodicBox(system, periodicBoxVectors);
            
            if (removeCMMotion)
                system.Particles.RemoveCentreOfMassMotion();

            step++;
            system.SimulationLogManager.Log(system, step, timeStep);

        }

        private void InitialiseCandidatePositions()
        {
            candidatePositions.Clear();
            candidatePositions.Capacity = system.NumberOfParticles;
            for (int i = 0; i < system.NumberOfParticles; i++)
            {
                candidatePositions.Add(Vector3.Zero);
            }
        }

        #endregion Public Methods

        #region Private Fields

        private bool iVarsInitialised;
        private IReporter reporter;
        private long step;
        private AtomicSystem system;
        private float timeStep;

        private IVariable<float> timeStepIVar;
        private float constraintTolerance = 0.0001f;
        private List<float> inverseMasses = new List<float>();
        private List<Vector3> candidatePositions = new List<Vector3>();
        private bool removeCMMotion;
        private Vector3[] periodicBoxVectors;

        #endregion Private Fields

        #region Private Methods

        /// <summary>
        /// Applies the forces.
        /// </summary>
        /// <param name="velocity">The velocity.</param>
        /// <param name="force">The force.</param>
        /// <param name="inverseMass">The inverse of masses.</param>
        /// <param name="ts">The time step.</param>
        private void ApplyForces(List<Vector3> velocity, List<Vector3> force, List<float> inverseMass, float ts)
        {
            for (int i = 0; i < velocity.Count; i++)
            {
                velocity[i] += ts * inverseMass[i] * force[i];
            }
        }

        /// <summary>
        /// Finds the first value in the vector that is NaN. Returns -1 if none found.
        /// </summary>
        /// <param name="vector"></param>
        /// <returns> Index of first NaN, or -1 if none found </returns>
        private int CheckForNaNs(List<Vector3> vector)
        {
            for (int index = 0; index < vector.Count; index++)
            {
                if (InvalidFloat(vector[index].X) || InvalidFloat(vector[index].Y) || InvalidFloat(vector[index].Z)) return index;
            }
            return -1;
        }

        private bool InvalidFloat(float x)
        {
            return float.IsInfinity(x) || float.IsNaN(x);
        }

        /// <summary>
        /// Propagates the positions.
        /// </summary>
        /// <param name="sys">The system.</param>
        /// <param name="newPositions">The new positions.</param>
        private void PropagatePositions(IAtomicSystem sys, List<Vector3> newPositions)
        {
            Vector3 newPosition, currentPosition;
            Vector3 v;
            ISimulationBoundary simBoundary = sys.Boundary;
            float dt = timeStep;
            bool periodic = simBoundary.PeriodicBoundary;

            if (periodic)
            {
                simBoundary.GetBoxLengths();
            }

            for (int i = 0; i < sys.NumberOfParticles; i++)
            {
                if (Math.Abs(sys.Particles.Mass[i]) > 0.0000001f)
                {
                    currentPosition = sys.Particles.Position[i];
                    v = sys.Particles.Velocity[i];

                    //Candidate new position
                    newPosition = currentPosition + v * dt;

                    newPositions[i] = newPosition;
                }
            }
        }

        /// <summary>
        /// Initialises the inverse masses.
        /// </summary>
        private void InitialiseInverseMasses()
        {
            inverseMasses.Clear();
            inverseMasses.Capacity = system.NumberOfParticles;
            foreach (float mass in system.Particles.Mass)
            {
                if (Math.Abs(mass) < 0.00001f)
                    inverseMasses.Add(mass);
                else
                    inverseMasses.Add(1.0f / mass);
            }
        }

        /// <summary>
        /// Applies distance constraints and updates velocities.
        /// </summary>
        private void ApplyConstraints()
        {

            SimulationBoundary simBoundary = system.Boundary as SimulationBoundary;
            if (simBoundary != null)
            {
                BoundingBox box = simBoundary.SimulationBox;
                bool periodic = simBoundary.PeriodicBoundary;
                bool resizing = simBoundary.Resizing;

                foreach (IConstraintImplementation constraint in system.Constraints)
                {
                    if(constraint.HasConstraints())
                        constraint.ApplyConstraintsToPositions(system.Particles.Position, candidatePositions, inverseMasses, constraintTolerance);
                }


                
                float velocityScale = 1.0f / timeStep;
                for (int i = 0; i < system.NumberOfParticles; i++)
                {
                    if (Math.Abs(system.Particles.Mass[i]) > 0.000001f)
                    {
                        Vector3 updateMask = new Vector3(1.0f, 1.0f, 1.0f);
                        // handle collisions with the edge of the box if not using periodic boundaries.
                        if (periodic == false || resizing)
                        {
                            Vector3 candidatePosition = candidatePositions[i];
                            Vector3 v = system.Particles.Velocity[i];

                            // keep current position and ivert velocity if it is outside of the box and goes away from the box
                            if (((candidatePosition.X <= box.Minimum.X) && (v.X <= 0.0f)) ||
                                ((candidatePosition.X >= box.Maximum.X) && (v.X >= 0.0f)))
                            {
                                updateMask.X = 0.0f;
                            }

                            if (((candidatePosition.Y <= box.Minimum.Y) && (v.Y <= 0.0f)) ||
                                ((candidatePosition.Y >= box.Maximum.Y) && (v.Y >= 0.0f)))
                            {
                                updateMask.Y = 0.0f;
                            }

                            if (((candidatePosition.Z <= box.Minimum.Z) && (v.Z <= 0.0f)) ||
                                ((candidatePosition.Z >= box.Maximum.Z) && (v.Z >= 0.0f)))
                            {
                                updateMask.Z = 0.0f;
                            }

                        }

                        Vector3 newVelocity = velocityScale * (candidatePositions[i] - system.Particles.Position[i]);

                        system.Particles.Velocity[i] =
                            Vector3.Modulate(newVelocity, updateMask)
                            - Vector3.Modulate(system.Particles.Velocity[i], Vector3.One - updateMask);

                        system.Particles.Position[i] =
                            Vector3.Modulate(candidatePositions[i], updateMask)
                            + Vector3.Modulate(system.Particles.Position[i], Vector3.One - updateMask);
                    }
                }
            }
        }

        #endregion Private Methods

        /// <summary>
        /// Gets the properties of this integrator.
        /// </summary>
        /// <returns>IIntegratorProperties.</returns>
        public IIntegratorProperties GetProperties()
        {
            VerletIntegratorProperties properties = new VerletIntegratorProperties()
            {
                TimeStep = timeStep,
                RemoveCmMotion = removeCMMotion
            };
            return properties;
        }

        public bool RemovesCmMotion()
        {
            return removeCMMotion;
        }
    }
}