﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using Nano.Science;
using Nano.Transport.Agents;
using Nano.Transport.Variables;

// ReSharper disable once CheckNamespace
namespace Narupa.MD.System
{
    partial class AtomicSystem
    {
        public event EventHandler PressureChanged;

        /// <summary>
        /// The desired pressure, in bars.
        /// </summary>
        public float DesiredPressureInBars;

        /// <summary>
        /// Gets the desired pressure.
        /// </summary>
        /// <value>The desired pressure in kJ/(mol * nm^3).</value>
        public float DesiredPressure
        {
            get { return DesiredPressureInBars * (PhysicalConstants.AvogadroC * 1e-25f); }
        }

        private IVariable<float> kineticEnergyIVar;
        private IVariable<int> numberOfParticlesIVar;
        private IVariable<float> potentialEnergyIVar;
        private IVariable<float> pressureIVar;
        private IVariable<int> numberOfMoleculesIVar;
        private IVariable<float> numberofMolsIVar;

        public override void ResetVariableDesiredValues()
        {
            base.ResetVariableDesiredValues();
            if (!VariablesInitialised) return;
            numberOfParticlesIVar.DesiredValue = NumberOfParticles;
            numberOfMoleculesIVar.DesiredValue = Topology.Molecules.Count;
            numberofMolsIVar.DesiredValue = Topology.Molecules.Count / PhysicalConstants.AvogadroC;
            pressureIVar.DesiredValue = AveragePressureInBars;
            DesiredPressureInBars = AveragePressureInBars;

            CalculateKineticEnergyAndTemperature();
            (Boundary as IVariableProvider).ResetVariableDesiredValues();
            (Thermostat as IVariableProvider)?.ResetVariableDesiredValues();
        }

        public override void InitialiseVariables(VariableManager variableManager)
        {
            base.InitialiseVariables(variableManager);
            numberofMolsIVar = variableManager.Variables.Add(VariableName.NumberOfMols, VariableType.Float, true) as IVariable<float>;
            kineticEnergyIVar = variableManager.Variables.Add(VariableName.KineticEnergy, VariableType.Float, true) as IVariable<float>;
            potentialEnergyIVar = variableManager.Variables.Add(VariableName.PotentialEnergy, VariableType.Float, true) as IVariable<float>;
            numberOfParticlesIVar = variableManager.Variables.Add(VariableName.NumberOfParticles, VariableType.Int32, true) as IVariable<int>;
            numberOfMoleculesIVar = variableManager.Variables.Add(VariableName.NumberOfMolecules, VariableType.Int32, true) as IVariable<int>;
            pressureIVar = variableManager.Variables.Add(VariableName.Pressure, VariableType.Float, true) as IVariable<float>;
            
            (Thermostat as IVariableProvider)?.InitialiseVariables(variableManager);
        }

        public override void UpdateVariableValues()
        {
            base.UpdateVariableValues();
            if (VariablesInitialised == false) return;
            kineticEnergyIVar.Value = KineticEnergy;
            potentialEnergyIVar.Value = PotentialEnergy;
            (Boundary as IVariableProvider).UpdateVariableValues();
            (Thermostat as IVariableProvider)?.UpdateVariableValues();

            if (pressureIVar.Value != AveragePressureInBars)
                pressureIVar.Value = AveragePressureInBars;
            if (pressureIVar.DesiredValue != pressureIVar.Value)
            {
                DesiredPressureInBars = pressureIVar.DesiredValue;
                PressureChanged?.Invoke(this, null);
            }
        }
    }
}