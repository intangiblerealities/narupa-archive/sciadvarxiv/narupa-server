﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using Nano;
using Nano.Loading;
using Nano.Science.Simulation;
using Nano.Science.Simulation.Log;
using Nano.Server.Basic;
using Nano.Transport.Variables.Interaction;
using Newtonsoft.Json;
using Formatting = Newtonsoft.Json.Formatting;
using Interaction = Nano.Science.Simulation.Interaction;

namespace Narupa.MD.Log
{
    [Flags]
    public enum InteractionLogOptions
    {
        /// <summary>
        /// Time (ps).
        /// </summary>
        Time = 1,

        /// <summary>
        /// A unique key string identifying the set of atoms in the interaction. This set is written to a separate file.
        /// </summary>
        SelectedAtomsKey = 2,

        PlayerID = 4,
        InputID = 8,
        Energy = 16,
        NetForce = 32,
        ForceMagnitude = 64,
        Position = 128,
        CentreOfMass = 256
    }

    [XmlName("Interaction")]
    public class InteractionLogger : ISimulationLogger
    {
        /// <summary>
        /// The observed sets of atoms interacted with. Value is a hashcode consisting of date-time stamp, number of atoms in the selection and hashcode for set.
        /// </summary>
        private Dictionary<EquatableSet, string> selectedAtomSets = new Dictionary<EquatableSet, string>();

        private Dictionary<string, EquatableSet> reversedKeyAtomSets = new Dictionary<string, EquatableSet>();

        /// <summary>
        /// The path in which this logger will create files
        /// </summary>
        protected string LogPath = "^/Logs/Trajectories";

        public bool IsInteractive {
            get;
            private set;
        }
        
        public InteractionLogOptions LogOptions = InteractionLogOptions.Time | InteractionLogOptions.SelectedAtomsKey |
                                                  InteractionLogOptions.PlayerID | InteractionLogOptions.InputID |
                                                  InteractionLogOptions.Energy | InteractionLogOptions.NetForce |
                                                  InteractionLogOptions.Position | InteractionLogOptions.CentreOfMass;

        /// <summary>
        /// How often, in time steps, will logs be written.
        /// </summary>
        protected int WriteFrequency = 1;

        /// <summary>
        /// If set to true, a date time stamp will be created with each log file.
        /// </summary>
        /// <remarks>
        /// If set to false, logs from previous runs may be overwritten.
        /// </remarks>
        protected bool UseDateTimeStamp = true;

        private bool logCreated;

        //TODO possibly generalise to CSVHelper?
        private StreamWriter fileWriter;

        private JsonWriter jsonWriter;

        private long step;

        private float timeStep;
        private float simulationTime;

        /// <inheritdoc />
        public string Identifier
        {
            get
            {
                return Helper.ResolvePath(LogPath + "/interactions");
            }

            set
            {
                throw new NotImplementedException();
            }
        }

        /// <inheritdoc />
        public ISimulationLogger Clone()
        {
            InteractionLogger newLogger = new InteractionLogger
            {
                LogOptions = LogOptions,
                LogPath = LogPath,
                UseDateTimeStamp = UseDateTimeStamp,
                WriteFrequency = WriteFrequency
            };

            return newLogger;
        }

        /// <inheritdoc />
        public void CloseLog()
        {
            fileWriter?.Close();
        }

        private string[] logIdentifiers;
        private bool selectedAtomSetsIsDirty;
        private string csvPath;

        /// <inheritdoc />
        public void CreateLog(bool append, params string[] logIdentifiers)
        {
            this.logIdentifiers = logIdentifiers;
            Helper.EnsurePathExists(Path.Combine(Helper.ResolvePath(LogPath), "dummy"));

            if (logCreated)
            {
                throw new Exception("Log already created!");
            }

            csvPath = SimulationLoggerBase.CreateLogFilePath(LogPath, "trajectory_interactions", logIdentifiers, UseDateTimeStamp, "csv");

            fileWriter = new StreamWriter(csvPath, append);

            
            if (append == false)
            {
                WriteCsvHeader();
            }

            logCreated = true;
        }

        private void WriteSelectionsToJson()
        {
            string jsonPath = Path.Combine(
                Path.GetDirectoryName(csvPath) ?? throw new NullReferenceException($"Invalid InteractionLogger path. {csvPath}"), 
                Path.GetFileNameWithoutExtension(csvPath) + "_selections.json");
            
            var selectionWriter = new StreamWriter(jsonPath);
            string jsonText = JsonConvert.SerializeObject(reversedKeyAtomSets, Formatting.Indented);
            //TODO perform writing async.
            selectionWriter.Write(jsonText);
            selectionWriter.Flush();
            selectionWriter.Dispose();
        }

        private void WriteCsvHeader()
        {
            string headerLine = "";
            headerLine += "timeStep";
            headerLine += (LogOptions & InteractionLogOptions.Time) == InteractionLogOptions.Time ? ", time (ps)" : "";
            headerLine += (LogOptions & InteractionLogOptions.PlayerID) == InteractionLogOptions.PlayerID ? ", player ID" : "";
            headerLine += (LogOptions & InteractionLogOptions.InputID) == InteractionLogOptions.InputID ? ", input ID" : "";
            headerLine += (LogOptions & InteractionLogOptions.SelectedAtomsKey) == InteractionLogOptions.SelectedAtomsKey ? ", atom" : "";
            headerLine += (LogOptions & InteractionLogOptions.Energy) == InteractionLogOptions.Energy ? ", energy (kJ/mol)" : "";
            headerLine += (LogOptions & InteractionLogOptions.NetForce) == InteractionLogOptions.NetForce ? ", net force X (kJ/(mol*nm)), , net force Y (kJ/(mol*nm)), , net force Z (kJ/(mol*nm))" : "";
            headerLine += (LogOptions & InteractionLogOptions.ForceMagnitude) == InteractionLogOptions.ForceMagnitude ? ", force magnitude (kJ/(mol*nm))" : "";
            headerLine += (LogOptions & InteractionLogOptions.Position) == InteractionLogOptions.Position ? ", interaction position X (nm), interaction position Y (nm), interaction position Z (nm) " : "";
            headerLine += (LogOptions & InteractionLogOptions.CentreOfMass) == InteractionLogOptions.CentreOfMass ? ", center of mass of selected atoms X (nm),  center of mass of selected atoms Y (nm),  center of mass of selected atoms Z (nm) " : "";

            fileWriter.WriteLine(headerLine);
        }

        /// <inheritdoc />
        public void Dispose()
        {
            if (fileWriter != null)
                fileWriter.Close();
        }

        /// <inheritdoc />
        public bool IsDueToLog(long step, float timeStep)
        {
            this.step = step;
            this.timeStep = timeStep;
            simulationTime = step * timeStep;
            if (step % WriteFrequency == 0)
                return true;
            else return false;
        }

        private string GetHashCodeForAtomSet(float time, ICollection<int> set)
        {
            var set1 = set as EquatableSet;
            var atomSet = set1 ?? new EquatableSet(set);

            if (selectedAtomSets.ContainsKey(atomSet))
                return selectedAtomSets[atomSet];
            else
            {
                selectedAtomSetsIsDirty = true;
                string hashcode;
                hashcode = simulationTime + "_" + set.Count + "_" + set.GetHashCode();
                selectedAtomSets.Add(atomSet, hashcode);
                reversedKeyAtomSets.Add(hashcode, atomSet);
                return hashcode;
            }
        }

        /// <inheritdoc />
        public void LogState(IAtomicSystem system)
        {
            selectedAtomSetsIsDirty = false;
            foreach (Interaction interaction in system.Interactions)
            {
                if (interaction.Type == InteractionType.NoInteraction)
                    continue;
                string line = "";

                line += step.ToString();
                line += (LogOptions & InteractionLogOptions.Time) == InteractionLogOptions.Time ? $", {simulationTime}" : "";
                line += (LogOptions & InteractionLogOptions.PlayerID) == InteractionLogOptions.PlayerID ? $", {interaction.PlayerId}" : "";
                line += (LogOptions & InteractionLogOptions.InputID) == InteractionLogOptions.InputID ? $", {interaction.InputId}" : "";

                var selectedAtomsKey = GetHashCodeForAtomSet(timeStep, interaction.SelectedAtoms);

                line += (LogOptions & InteractionLogOptions.SelectedAtomsKey) == InteractionLogOptions.SelectedAtomsKey ? $", {selectedAtomsKey}" : "";
                line += (LogOptions & InteractionLogOptions.Energy) == InteractionLogOptions.Energy ? $", {interaction.Energy}" : "";
                if ((LogOptions & InteractionLogOptions.NetForce) == InteractionLogOptions.NetForce)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        line += $", {interaction.NetForce[i]}";
                    }
                }
                line += (LogOptions & InteractionLogOptions.ForceMagnitude) == InteractionLogOptions.ForceMagnitude ? $", {interaction.ForceMagnitude}" : "";
                if ((LogOptions & InteractionLogOptions.Position) == InteractionLogOptions.Position)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        line += $", {interaction.Position[i]}";
                    }
                }
                if ((LogOptions & InteractionLogOptions.CentreOfMass) == InteractionLogOptions.CentreOfMass)
                {
                    for (int i = 0; i < 3; i++)
                    {
                        line += $", {interaction.SelectedAtomsCentreOfMass[i]}";
                    }
                }
                fileWriter.WriteLine(line);
            }

            fileWriter.Flush();

            if (selectedAtomSetsIsDirty)
                WriteSelectionsToJson();
        }

        /// <inheritdoc />
        public virtual void Load(LoadContext context, XmlNode node)
        {
            LogPath = Helper.ResolvePath(Helper.GetAttributeValue(node, "LogPath", LogPath));
            LogOptions = LoadInteractionLogOptions(context, node);
            WriteFrequency = Helper.GetAttributeValue(node, "WriteFrequency", WriteFrequency);
            UseDateTimeStamp = Helper.GetAttributeValue(node, "UseDateTimeStamp", UseDateTimeStamp);
            IsInteractive = Helper.GetAttributeValue(node, "Interactive", IsInteractive);

        }

        private InteractionLogOptions LoadInteractionLogOptions(LoadContext context, XmlNode node)
        {
            //If the user understands bitmasks, they can dump a value directly in
            InteractionLogOptions logOptions = Helper.GetAttributeValue(node, "InteractionLogOptions", LogOptions);
            //Otherwise, if an option is set then add it to the bitmask.
            logOptions |= Helper.GetAttributeValue(node, "Time", true) ? InteractionLogOptions.Time : logOptions;
            logOptions |= Helper.GetAttributeValue(node, "PlayerID", true) ? InteractionLogOptions.PlayerID : logOptions;
            logOptions |= Helper.GetAttributeValue(node, "SelectedAtoms", true) ? InteractionLogOptions.SelectedAtomsKey : logOptions;
            logOptions |= Helper.GetAttributeValue(node, "InputID", true) ? InteractionLogOptions.InputID : logOptions;
            logOptions |= Helper.GetAttributeValue(node, "Energy", true) ? InteractionLogOptions.Energy : logOptions;
            logOptions |= Helper.GetAttributeValue(node, "NetForce", true) ? InteractionLogOptions.NetForce : logOptions;
            logOptions |= Helper.GetAttributeValue(node, "ForceMagnitude", false) ? InteractionLogOptions.ForceMagnitude : logOptions;
            logOptions |= Helper.GetAttributeValue(node, "Position", true)
                ? InteractionLogOptions.Position
                : logOptions;
            logOptions |= Helper.GetAttributeValue(node, "SelectedAtomsCOM", true)
                ? InteractionLogOptions.CentreOfMass
                : logOptions;

            return logOptions;
        }

        /// <inheritdoc />
        public virtual XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "LogPath", LogPath);
            Helper.AppendAttributeAndValue(element, "InteractionLogOptions", LogOptions);
            Helper.AppendAttributeAndValue(element, "WriteFrequency", WriteFrequency);
            Helper.AppendAttributeAndValue(element, "UseDateTimeStamp", UseDateTimeStamp);
            Helper.AppendAttributeAndValue(element, "Interactive", IsInteractive);

            return element;
        }
    }
}