﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Net;
using Nano.Transport.Agents;
using Nano.Transport.Streams;

namespace Nano.Server.Basic
{
    public class ServerSyncBuffer<T>
    {
        /// <summary>
        /// ID of the stream.
        /// </summary>
        public readonly ushort ID;

        public readonly IPEndPoint IPEndPoint;

        /// <summary>
        /// Sync lock object.
        /// </summary>
        public readonly object Lock = new object();

        public EventHandler StreamUpdated;

        /// <summary>
        /// Gets the count.
        /// </summary>
        /// <value>The count.</value>
        public int Count { get; private set; }

        /// <summary>
        /// Gets the data.
        /// </summary>
        /// <value>The data.</value>
        public T[] Data { get; private set; }

        /// <summary>
        /// Gets a flag indicating that the buffer has been updated and needs to be copied.
        /// </summary>
        public bool IsDirty { get; internal set; }

        /// <summary>
        /// Gets the data stream instance.
        /// </summary>
        public IDataStream<T> Stream { get; private set; }

        public ServerSyncBuffer(IPEndPoint ipEndPoint, ushort id)
        {
            IPEndPoint = ipEndPoint;
            ID = id;
        }

        public void Attach(ServerBase server)
        {
            server.IncomingStreams[IPEndPoint].Added += Inputs_Added;
            server.IncomingStreams[IPEndPoint].Removed += Inputs_Removed;
        }

        public void Detach(ServerBase server)
        {
            server.IncomingStreams[IPEndPoint].Added -= Inputs_Added;
            server.IncomingStreams[IPEndPoint].Removed -= Inputs_Removed;
        }

        private void Inputs_Added(IncomingDataAgent agent, ushort streamID, TransportDataStreamReceiver dataStreamReceiver)
        {
            if (streamID != ID)
            {
                return;
            }

            dataStreamReceiver.StreamCreated += OnStreamCreated;
            dataStreamReceiver.StreamDisposed += OnStreamDisposed;
        }

        private void Inputs_Removed(IncomingDataAgent agent, ushort streamID, TransportDataStreamReceiver dataStreamReceiver)
        {
            if (streamID != ID)
            {
                return;
            }

            dataStreamReceiver.StreamCreated -= OnStreamCreated;
            dataStreamReceiver.StreamDisposed -= OnStreamDisposed;
        }

        private void OnStreamCreated(TransportDataStreamReceiver receiver, ITransportDataStream stream)
        {
            lock (Lock)
            {
                Stream = stream as IDataStream<T>;
                Data = new T[stream.MaxCount];
            }

            receiver.Updated += OnUpdated;
        }

        private void OnStreamDisposed(TransportDataStreamReceiver receiver, ITransportDataStream stream)
        {
            receiver.Updated -= OnUpdated;

            Stream = null;
        }

        private void OnUpdated(object sender, EventArgs e)
        {
            StreamUpdated?.Invoke(this, null);

            if (Stream == null)
            {
                return;
            }

            Count = Stream.Count;

            Array.ConstrainedCopy(Stream.Data, 0, Data, 0, Stream.Count);

            IsDirty = true;
        }
    }
}