﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace Nano.Science.Simulation.Integrator
{
    /// <summary>
    /// Interface for a molecular dynamics Integrator
    /// </summary>
    public interface IIntegrator
    {
        /// <summary>
        /// Propagates an atomic system by the specfied number of steps.
        /// </summary>
        void Propagate(int steps);

        /// <summary>
        /// Gets the time step.
        /// </summary>
        /// <returns>System.Single.</returns>
        float GetTimeStep();

        /// <summary>
        /// Resets the integrator.
        /// </summary>
        /// <param name="activeSystem">The active system.</param>
        void ResetIntegrator(IAtomicSystem activeSystem);

        /// <summary>
        /// Gets the properties of this integrator.
        /// </summary>
        /// <returns>IIntegratorProperties.</returns>
        IIntegratorProperties GetProperties();

        /// <summary>
        /// Whether this integrator will remove centre of mass motion.
        /// </summary>
        /// <returns></returns>
        bool RemovesCmMotion();
    }
}