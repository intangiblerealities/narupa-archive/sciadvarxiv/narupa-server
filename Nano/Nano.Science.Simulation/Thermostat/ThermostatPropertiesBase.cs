﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using Nano.Loading;
using System.Xml;
using Nano.Science.Simulation.Integrator;

namespace Nano.Science.Simulation
{
    /// <summary>
    /// Base representation of thermostat properties.
    /// </summary>
    /// <seealso cref="Nano.Science.Simulation.IThermostatProperties" />
    [XmlName("ThermostatBase")]
    public class ThermostatPropertiesBase : IThermostatProperties
    {
        /// <summary>
        /// The equilibrium temperature of this thermostat.
        /// </summary>
        public float EquilibriumTemperature;
        /// <summary>
        /// The maximum temperature this thermostat will allow.
        /// </summary>
        public float MaximumTemperature;
        /// <summary>
        /// The minimum temperature this thermostat will allow.
        /// </summary>
        public float MinimumTemperature;

        /// <inheritdoc />
        public virtual string Name
        {
            get { return "ThermostatBase"; }
        }

        /// <inheritdoc />
        public virtual IThermostatProperties Clone()
        {
            ThermostatPropertiesBase therm = new ThermostatPropertiesBase
            {
                EquilibriumTemperature = EquilibriumTemperature,
                MaximumTemperature = MaximumTemperature,
                MinimumTemperature = MinimumTemperature
            };
            return therm;
        }

        /// <inheritdoc />
        public virtual bool Equals(IThermostatProperties other)
        {
            if (other is ThermostatPropertiesBase == false) return false;
            ThermostatPropertiesBase therm = other as ThermostatPropertiesBase;
            if (therm.EquilibriumTemperature != EquilibriumTemperature) return false;
            if (therm.MaximumTemperature != MaximumTemperature) return false;
            if (therm.MinimumTemperature != MinimumTemperature) return false;
            if (therm.Name != Name) return false;
            return true;
        }

        /// <inheritdoc />
        public virtual IThermostat InitialiseStat(IAtomicSystem system, IIntegrator integrator)
        {
            throw new Exception("Cannot instatiate base thermostat.");
        }

        /// <inheritdoc />
        public virtual void Load(LoadContext context, XmlNode node)
        {
            EquilibriumTemperature = Helper.GetAttributeValue(node, "EquilibriumTemperature", 300f);
            MaximumTemperature = Helper.GetAttributeValue(node, "MaximumTemperature", 10000f);
            MinimumTemperature = Helper.GetAttributeValue(node, "MinimumTemperature", 0.01f);
        }

        /// <inheritdoc />
        public virtual XmlElement Save(LoadContext context, XmlElement element)
        {
            Helper.AppendAttributeAndValue(element, "EquilibriumTemperature", EquilibriumTemperature);
            Helper.AppendAttributeAndValue(element, "MaximumTemperature", MaximumTemperature);
            Helper.AppendAttributeAndValue(element, "EquilibriumTemperature", EquilibriumTemperature);

            return element;
        }
    }
}