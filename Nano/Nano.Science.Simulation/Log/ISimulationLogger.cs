﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Loading;

namespace Nano.Science.Simulation.Log
{
    /// <summary>
    /// Interface for simulation logs.
    /// </summary>
    public interface ISimulationLogger : ILoadable
    {
        /// <summary>
        /// Identifier that will uniquely identify a logger
        /// </summary>
        /// <remarks>
        /// Used to prevent loggers creating conflicting files.
        /// @review @mike Not sure if this is the best way to do this.
        /// </remarks>
        string Identifier { get; set; }

        /// <summary>
        /// Initialise logging.
        /// </summary>
        /// <param name="append"></param>
        /// <param name="logIdentifiers"></param>
        /// <remarks>
        /// Additional identifiers can be provided which will be appended to the filenames produced by this logger.
        /// For example, one may pass a player ID and a datetime string so logs produced by different players are stored separately.
        /// </remarks>
        void CreateLog(bool append, params string[] logIdentifiers);

        /// <summary>
        /// Whether or not the logger should log now, based on the current step.
        /// </summary>
        /// <param name="step">The current step.</param>
        /// <param name="timeStep">Size (in ps) of time step.</param>
        bool IsDueToLog(long step, float timeStep);

        /// <summary>
        /// Whether this logger will be controlled interactively.
        /// </summary>
        bool IsInteractive { get; }
        
        
        /// <summary>
        /// Given the current state of the system, log it.
        /// </summary>
        /// <param name="system"></param>
        void LogState(IAtomicSystem system);

        /// <summary>
        /// Closes open files.
        /// </summary>
        void CloseLog();

        /// <summary>
        /// Dispose of this instance.
        /// </summary>
        void Dispose();

        /// <summary>
        /// Clones this instance.
        /// </summary>
        /// <returns>Clone of this instance.</returns>
        ISimulationLogger Clone();
    }
}