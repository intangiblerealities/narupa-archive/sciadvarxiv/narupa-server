// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using Rug.Osc;

namespace Nano.Science.Simulation.Instantiated
{
    /// <summary>
    /// Interface for server side atom selections.
    /// </summary>
    public interface IAtomSelectionPool
    {
        /// <summary>
        /// The root address of the selection pool.
        /// </summary>
        OscAddress Address { get; }

        /// <summary>
        /// Occurs when the visible atoms have changed.
        /// </summary>
        event EventHandler VisibleAtomsChanged;

        /// <summary>
        /// The list of currently visible atoms.
        /// </summary>
        List<int> VisibleAtoms { get; set; }

        /// <summary>
        /// Updates the set of visible atoms.
        /// </summary>
        void UpdateVisibleAtoms();
    }
}