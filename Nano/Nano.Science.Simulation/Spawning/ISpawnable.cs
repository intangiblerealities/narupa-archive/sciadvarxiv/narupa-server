﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System.Collections.Generic;
using Nano.Science.Simulation.Instantiated;
using SlimMath;

namespace Nano.Science.Simulation.Spawning
{
    /// <summary>
    /// Interface for spawnable topology content.
    /// </summary>
    /// <autogeneratedoc />
    public interface ISpawnable
    {
        /// <summary>
        /// The name of this spawnable object.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Whether the spawnable will intersect with the given bounding spheres.
        /// </summary>
        /// <param name="boundingSpheres">List of bounding spheres of the currently spawned topology.</param>
        /// <param name="context">Spawn context.</param>
        bool Intersects(List<BoundingSphere> boundingSpheres, ref SpawnContext context);

        /// <summary>
        /// Spawns the specified topology.
        /// </summary>
        /// <param name="box">The simulation bounding box.</param>
        /// <param name="boundingSpheres">List of bounding spheres in the currently spawned topology.</param>
        /// <param name="topology">The topology.</param>
        /// <param name="context">The context.</param>
        /// <autogeneratedoc />
        void Spawn(ISimulationBoundary box, List<BoundingSphere> boundingSpheres, InstantiatedTopology topology, ref SpawnContext context);

        /// <summary>
        /// Gets the bounding box of the spawnable.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <returns>BoundingBox.</returns>
        BoundingBox GetBoundingBox(ref SpawnContext context);
    }
}