﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the ^GPL. See License.txt in the project root for license information.

namespace Nano.Rainbow.Private.v1_0
{
    public class ReservationState
    {
        public static readonly string MethodPath = "/1.0/server/reservation";

        public string SecretKey { get; set; }

        public ConnectionSessionState SessionState { get; set; }
    }
}