﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Commands;
using Nano.Transport.Agents;
using Nano.Transport.Comms;
using Nano.Transport.Streams;
using Nano.Transport.Variables;
using Nano.Transport.Variables.Interaction;
using Rug.Osc;
using SlimMath;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Runtime.InteropServices;
// ReSharper disable InconsistentNaming

namespace Nano.Client.Console.Simbox
{
    public class Client : ClientBase
    {
        [Obsolete("InputFields is deprecated, use Interactions stream instead.")]
        public readonly IDataStream<Vector3> InputFields;

        public readonly IDataStream<Nano.Transport.Variables.Interaction.Interaction> Interactions;

        /// <summary>
        /// Stream for transmitting the local VR device positions to the server.
        /// </summary>
        public readonly IDataStream<Nano.Transport.Variables.VRInteraction> LocalVrDevicePositions;

        /// <summary>
        /// Stream for transmitting the atoms that should be associated with an interaction in the Interactions stream.
        /// </summary>
        public readonly IDataStream<int> InteractionAtoms;

       public Client(ITransportPacketTransmitter transmitter, PacketStatistics packetStatistics,
            IReporter reporter, bool inMemory) : base(transmitter, packetStatistics, reporter, inMemory)
        {
            int count = 32; // 2048
            int maxNumberOfAtoms = 1000000;
            InputFields = OutgoingStreams.DefineStream<Vector3>(1, "Input Fields", VariableType.Float, 3, count,
                TransportDataStreamTransmitMode.Continuous);
            Interactions = OutgoingStreams.DefineStream<Interaction>((ushort)StreamID.Interaction, "Interaction",
                VariableType.Byte, Marshal.SizeOf(typeof(Interaction)), 8 * 4,
                TransportDataStreamTransmitMode.Continuous);
            LocalVrDevicePositions = OutgoingStreams.DefineStream<Nano.Transport.Variables.VRInteraction>(
                (ushort)StreamID.VRInteraction, "SteamVR Interaction", VariableType.Byte,
                Marshal.SizeOf(typeof(Nano.Transport.Variables.VRInteraction)), 8 * 4,
                TransportDataStreamTransmitMode.Continuous);
            InteractionAtoms = OutgoingStreams.DefineStream<int>((ushort)StreamID.InteractionSelectedAtoms,
                "Interaction Selected Atoms", VariableType.Int32, 1, maxNumberOfAtoms,
                TransportDataStreamTransmitMode.Continuous);
        }

        public static string GetLocalIPAddress()
        {
            var host = Dns.GetHostEntry(Dns.GetHostName());
            foreach (var ip in host.AddressList)
            {
                if (ip.AddressFamily == AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }
            }
            throw new Exception("Local IP Address Not Found!");
        }

        public void RequestJoinAllStreams()
        {
            foreach (ushort id in new List<ushort>(IncomingStreams.Keys))
            {
                IncomingStreams.JoinStream(id);
            }
        }

        public void RequestLeaveAllStreams()
        {
            foreach (ushort id in new List<ushort>(IncomingStreams.Keys))
            {
                IncomingStreams.LeaveStream(id);
            }
        }

        public void RequestStreamList()
        {
            IncomingStreams.RequestStreamList();
        }

        public void StartXYZLogging()
        {
            int id = IPAddress.Parse(GetLocalIPAddress()).GetAddressBytes()[3];
            TransportContext.Transmitter.Broadcast(TransportPacketPriority.Critical, new OscMessage("/log/startXYZLog", id, 1, 1));
        }

        public void StopXYZLogging()
        {
            int id = IPAddress.Parse(GetLocalIPAddress()).GetAddressBytes()[3];
            TransportContext.Transmitter.Broadcast(TransportPacketPriority.Critical, new OscMessage("/log/stopXYZLog", id));
        }

        internal void SendPause()
        {
            TransportContext.Transmitter.Broadcast(TransportPacketPriority.Critical, new OscMessage(SimboxCommands.GetAddress(SimboxCommand.Pause)));
        }

        internal void SendPlay()
        {
            TransportContext.Transmitter.Broadcast(TransportPacketPriority.Critical, new OscMessage(SimboxCommands.GetAddress(SimboxCommand.Play)));
        }

        internal void SendRestart()
        {
            TransportContext.Transmitter.Broadcast(TransportPacketPriority.Critical, new OscMessage(SimboxCommands.GetAddress(SimboxCommand.Restart)));
        }

        internal void SendStep()
        {
            TransportContext.Transmitter.Broadcast(TransportPacketPriority.Critical, new OscMessage(SimboxCommands.GetAddress(SimboxCommand.Step)));
        }

        protected override IncomingDataAgent CreateDataReader()
        {
            return new ContextIncomingDataAgent(TransportContext);
        }
    }
}