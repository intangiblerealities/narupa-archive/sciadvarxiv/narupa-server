﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Net;
using Nano.Transport.Comms;

namespace Nano.Client
{
    public abstract class SimboxConnectionInfo : IComparable<SimboxConnectionInfo>, IEquatable<SimboxConnectionInfo>
    {
        public abstract IPAddress Address { get; }

        public abstract SimboxConnectionType ConnectionType { get; }

        public string Descriptor { get; }

        public abstract int Port { get; }

        public abstract ConnectionString ConnectionString { get; }

        internal SimboxConnectionInfo(string descriptor)
        {
            Descriptor = descriptor;
        }

        public int CompareTo(SimboxConnectionInfo other)
        {
            return string.Compare(Descriptor, other.Descriptor, StringComparison.Ordinal);
        }

        public override int GetHashCode()
        {
            return ToString().GetHashCode();
        }

        public abstract bool Equals(SimboxConnectionInfo other);
    }

    public class CustomConnectionInfo : SimboxConnectionInfo
    {
        /// <inheritdoc />
        public CustomConnectionInfo(string descriptor) : base(descriptor)
        {
        }

        /// <inheritdoc />
        public override IPAddress Address { get; } = IPAddress.Any;

        /// <inheritdoc />
        public override SimboxConnectionType ConnectionType { get; } = SimboxConnectionType.InMemory;

        /// <inheritdoc />
        public override int Port { get; } = 0;

        /// <inheritdoc />
        public override ConnectionString ConnectionString { get; } = null; 

        /// <inheritdoc />
        public override bool Equals(SimboxConnectionInfo other)
        {
            return other is CustomConnectionInfo && Descriptor == other.Descriptor; 
        }
    }
}