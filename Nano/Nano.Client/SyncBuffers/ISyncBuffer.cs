﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Transport.Agents;

namespace Nano.Client
{
    public interface ISyncBuffer<T>
    {
        event SyncBufferEvent<T> Updated;

        int Count { get; }
        T[] Data { get; }
        bool IsDirty { get; }

        void Attach(IncomingDataAgent agent);

        void Detach(IncomingDataAgent agent);
    }
}