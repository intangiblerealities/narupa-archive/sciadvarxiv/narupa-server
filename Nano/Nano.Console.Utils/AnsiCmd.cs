// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;
using System.IO;
using Rug.Cmd;

namespace Nano.Console.Utils
{
    public class AnsiCmd
    {
        public enum TextFormatting
        {
            Normal = 0, 
            Bold = 1,
            Faint = 2,
            Italic = 3, 
            Underline = 4, 
            SlowBlink = 5,
            RapidBlink = 6, 
            Reverse = 7, 
            Strikethrough = 9,             
        }

        public static IReporter CreateDefaultConsoleContext()
        {
            RC.Sys = ConsoleExt.SystemConsole;
            RC.App = RC.Sys;
            RC.Theme = Rug.Cmd.Colors.ConsoleColorTheme.Load(
                (ConsoleColor) (Math.Min((int) ConsoleColor.White,
                        Math.Max((int) ConsoleColor.Black,
                            (int) System.Console.BackgroundColor
                        )
                    )
                ), RugCmd.GetThemeStream());
            Colors.Heading = (ConsoleColor)RC.Theme[ConsoleThemeColor.TitleText];
            Colors.Emphasized = (ConsoleColor)RC.Theme[ConsoleThemeColor.TitleText2];
            Colors.UserInput = (ConsoleColor)RC.Theme[ConsoleThemeColor.Text];
            Colors.Ident = (ConsoleColor)RC.Theme[ConsoleThemeColor.Text2];
            Colors.Normal = (ConsoleColor)RC.Theme[ConsoleThemeColor.TitleText];
            Colors.Success = (ConsoleColor)RC.Theme[ConsoleThemeColor.TextGood];
            Colors.Error = (ConsoleColor)RC.Theme[ConsoleThemeColor.ErrorColor1];

            Colors.ErrorDetail = (ConsoleColor)RC.Theme[ConsoleThemeColor.ErrorColor2];
           
            Colors.Transmit = (ConsoleColor)RC.Theme[ConsoleThemeColor.SubTextGood];
            Colors.Receive = (ConsoleColor)RC.Theme[ConsoleThemeColor.SubTextBad];
            Colors.Message = (ConsoleColor)RC.Theme[ConsoleThemeColor.Text];
            Colors.Action = (ConsoleColor)RC.Theme[ConsoleThemeColor.Text3];
            Colors.Debug = (ConsoleColor)RC.Theme[ConsoleThemeColor.PromptColor1];
            Colors.Warning = (ConsoleColor)RC.Theme[ConsoleThemeColor.WarningColor1];
            AnsiCmdReporter reporter = new AnsiCmdReporter();
            
            return reporter;
        }
      
        public static readonly AnsiCmd Out = GetOutput();
        
        public static readonly AnsiCmd Error = GetError();

        private readonly object syncLock = new object();

        public static bool InsetActions { get; set; } = true; 

        public ConsoleColor OriginalForegroundColor { get; }

        public TextWriter Writer { get; }

        private AnsiCmd(TextWriter writer)
        {
            Writer = writer;

            OriginalForegroundColor = System.Console.ForegroundColor;
        }

        public void WriteException(string message, Exception exception, bool fullError = false)
        {
            lock (syncLock)
            {
                WriteLineToWriter(Colors.Error, TextFormatting.RapidBlink, message);

                if (fullError == true)
                {
                    WriteExceptionToWrite(exception);
                }
                else
                {
                    WriteLineToWriter(Colors.ErrorDetail, TextFormatting.Normal, exception.Message);
                }
            }
        }

        private void WriteExceptionToWrite(Exception exception)
        {
            WriteLineToWriter(Colors.Error, TextFormatting.Normal, exception.Message);

            WriteStackTraceToWriter(exception.StackTrace);
        }

        /// <summary>
        /// Write a stack trace string to the console
        /// </summary>
        /// <param name="trace">trace string</param>
        private void WriteStackTraceToWriter(string trace)
        {
            if (string.IsNullOrEmpty(trace) != false)
            {
                return;
            }
            
            Writer.WriteLine(); 
            WriteLineToWriter(Colors.ErrorDetail, TextFormatting.Normal, new string('=', 80));
            WriteLineToWriter(Colors.Normal, TextFormatting.Normal, "  " + trace.Replace("\n", "\n  "));
            Writer.WriteLine(); 

            WriteLineToWriter(Colors.ErrorDetail, TextFormatting.Normal, new string('=', 80));
            Writer.WriteLine();
        }
 
        public void WriteLine(ConsoleColor color, TextFormatting formatting, string message)
        {
            WriteLineToWriter(color, formatting, message);
        }

        public void WriteLine(ConsoleColor color, string message)
        {
            WriteLineToWriter(color, TextFormatting.Normal, message);
        }

        public void WriteMessage(Direction direction, ConsoleColor identColor, string ident, ConsoleColor messageColor, string message)
        {
            WriteMessage(direction, identColor, ident, messageColor, TextFormatting.Normal, message);
        }

        public void WriteMessage(Direction direction, ConsoleColor identColor, string ident, ConsoleColor messageColor, TextFormatting formatting, string message)
        {
            lock (syncLock)
            {
                switch (direction)
                {
                    case Direction.Transmit:
                        WriteToWriter(Colors.Transmit, TextFormatting.Normal, "TX ");
                        break;

                    case Direction.Receive:
                        WriteToWriter(Colors.Receive, TextFormatting.Normal, "RX ");
                        break;

                    case Direction.Action:
                        if (InsetActions)
                        {
                            WriteToWriter(Colors.Action, TextFormatting.Normal, "   ");
                        }
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(direction), direction, null);
                }

                if (string.IsNullOrEmpty(ident) == false)
                {
                    WriteToWriter(identColor, TextFormatting.Normal, $"{ident} ");
                }

                WriteLineToWriter(messageColor, formatting, message);
            }
        }

        private void WriteLineToWriter(ConsoleColor color, TextFormatting formatting, string message)
        {
            WriteToWriter(color, formatting, message);

            Writer.WriteLine(); 
        }

        private void WriteToWriter(ConsoleColor color, TextFormatting formatting, string message)
        {
            SetColor(color);

            if (formatting != TextFormatting.Normal) Writer.Write($"\x1B[{(int)formatting}m"); 
            
            Writer.Write(message);

            if (formatting != TextFormatting.Normal) Writer.Write("\x1B[0m");  
        }

        private static AnsiCmd GetError()
        {
            return new AnsiCmd(System.Console.Error);
        }

        private static AnsiCmd GetOutput()
        {
            return new AnsiCmd(System.Console.Out);
        }

        private void SetColor(ConsoleColor color)
        {
            System.Console.ForegroundColor = color; 
        }
    }
}