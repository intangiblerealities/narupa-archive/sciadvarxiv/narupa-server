﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Nano.Loading
{
    /// <summary>
    /// Load manager class.
    /// </summary>
    /// <autogeneratedoc />
    public class LoadManager
    {
        private static readonly string loadableTypeName = typeof(ILoadable).FullName; 

        private static readonly Dictionary<Type, Dictionary<string, Type>> typeLookup = new Dictionary<Type, Dictionary<string, Type>>();

        private static readonly List<LoadableType> allTypes = new List<LoadableType>();

        private static readonly Dictionary<Type, string> typeToXmlNameLookup = new Dictionary<Type, string>();

        static LoadManager()
        {
            ScanAssembly(typeof(LoadManager).Assembly);
        }

        /// <summary>
        /// Gets the all compatible <see cref="ILoadable"/> types for the specified base type.
        /// </summary>
        /// <param name="baseType">The base type.</param>
        /// <returns>Dictionary&lt;System.String, Type&gt;.</returns>
        /// <autogeneratedoc />
        public static Dictionary<string, Type> GetTypeOfType(Type baseType)
        {
            if (typeLookup.ContainsKey(baseType))
            {
                return typeLookup[baseType];
            }

            Dictionary<string, Type> types = new Dictionary<string, Type>();

            bool inclusive = true;

            if (baseType.IsAbstract || baseType.IsInterface)
            {
                inclusive = false;
            }

            foreach (LoadableType type in allTypes)
            {
                if ((type.Type.IsSubclassOf(baseType)) ||
                    (baseType.IsInterface && type.Type.GetInterfaces().Contains(baseType)) ||
                    (inclusive && (type.Type == baseType)))
                {
                    types.Add(type.XmlName, type.Type);
                }
            }

            typeLookup.Add(baseType, types);

            return types;
        }

        /// <summary>
        /// Scans the assembly for <see cref="ILoadable"/> types.
        /// </summary>
        /// <param name="assembly">The assembly to scan.</param>
        /// <autogeneratedoc />
        public static void ScanAssembly(Assembly assembly)
        {
            foreach (Type type in assembly.GetTypes())
            {
                if (type.GetInterface(loadableTypeName) != null) // .IsSubclassOf(typeof(ILoadable)))
                {
                    string name = type.Name;

                    object[] attribs = type.GetCustomAttributes(typeof(XmlNameAttribute), false);

                    if (attribs.Length > 0)
                    {
                        name = (attribs[0] as XmlNameAttribute).Name;
                    }
                    //else
                    //{
                    //    continue; 
                    //}

                    LoadableType t = new LoadableType();
                    t.Type = type;
                    t.XmlName = name;

                    if (!allTypes.Contains(t))
                    {
                        allTypes.Add(t);
                    }

                    if (typeToXmlNameLookup.ContainsKey(type) == false)
                    {
                        typeToXmlNameLookup.Add(type, name); 
                    }
                }
            }
        }

        /// <summary>
        /// Struct for loadable type meta data.
        /// </summary>
        /// <autogeneratedoc />
        private struct LoadableType
        {
            /// <summary>
            /// The type.
            /// </summary>
            /// <autogeneratedoc />
            public Type Type;

            /// <summary>
            /// The XML name.
            /// </summary>
            /// <autogeneratedoc />
            public string XmlName;

            /// <summary>
            /// Determines whether the specified <see cref="System.Object" /> is equal to this instance.
            /// </summary>
            /// <param name="obj">The object to compare with the current instance.</param>
            /// <returns><c>true</c> if the specified <see cref="System.Object" /> is equal to this instance; otherwise, <c>false</c>.</returns>
            /// <autogeneratedoc />
            public override bool Equals(object obj)
            {
                return ToString().Equals(obj.ToString());
            }

            /// <summary>
            /// Returns a hash code for this instance.
            /// </summary>
            /// <returns>A hash code for this instance, suitable for use in hashing algorithms and data structures like a hash table.</returns>
            /// <autogeneratedoc />
            public override int GetHashCode()
            {
                return ToString().GetHashCode();
            }

            /// <summary>
            /// Returns a <see cref="System.String" /> that represents this instance.
            /// </summary>
            /// <returns>A <see cref="System.String" /> that represents this instance.</returns>
            /// <autogeneratedoc />
            public override string ToString()
            {
                return $"{XmlName}:{Type.FullName}";
            }
        }

        /// <summary>
        /// Gets the XML name of the type.
        /// </summary>
        /// <param name="type">The type.</param>
        /// <returns>System.String.</returns>
        /// <exception cref="System.Collections.Generic.KeyNotFoundException">No loadable type information could be found for the specified type.</exception>
        /// <autogeneratedoc />
        public static string GetXmlName(Type type)
        {
            string name;

            if (typeToXmlNameLookup.TryGetValue(type, out name) == false)
            {
                throw new KeyNotFoundException($"No loadable type information could be found for the type \"{type.Name}\"."); 
            }

            return name; 
        }
    }

    /// <summary>
    /// XML load name attribute.
    /// </summary>
    /// <autogeneratedoc />
    public class XmlNameAttribute : Attribute
    {
        /// <summary>
        /// The name of the XML type.
        /// </summary>
        /// <autogeneratedoc />
        public readonly string Name;

        /// <summary>
        /// Initializes a new instance of the <see cref="XmlNameAttribute"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <autogeneratedoc />
        public XmlNameAttribute(string name)
        {
            Name = name; 
        }
    }
}
