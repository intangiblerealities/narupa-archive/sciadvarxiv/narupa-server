﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using System;
using System.Net;
using Rug.Osc;

namespace Nano
{
    public class NullReporter : IReporter
    {
        /// <inheritdoc />
        public IOscMessageFilter OscMessageFilter { get; set; }

        /// <inheritdoc />
        public ReportVerbosity ReportVerbosity { get; set; }

        /// <inheritdoc />
        public void PrintNormal(string format, params object[] args)
        {
        }

        /// <inheritdoc />
        public void PrintNormal(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {         
        }

        /// <inheritdoc />
        public void PrintNormal(Direction direction, string ident, string format, params object[] args)
        {         
        }

        /// <inheritdoc />
        public void PrintDetail(string format, params object[] args)
        {
        }

        /// <inheritdoc />
        public void PrintDetail(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
        }

        /// <inheritdoc />
        public void PrintDetail(Direction direction, string ident, string format, params object[] args)
        {
        }

        /// <inheritdoc />
        public void PrintDebug(string format, params object[] args)
        {
        }

        /// <inheritdoc />
        public void PrintDebug(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
        }

        /// <inheritdoc />
        public void PrintDebug(Direction direction, string ident, string format, params object[] args)
        {
        }

        /// <inheritdoc />
        public void PrintBlankLine(ReportVerbosity verbosity)
        {
        }

        /// <inheritdoc />
        public void PrintHeading(string format, params object[] args)
        {

        }

        /// <inheritdoc />
        public void PrintEmphasized(string format, params object[] args)
        {
        }

        /// <inheritdoc />
        public void PrintEmphasized(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
        }

        /// <inheritdoc />
        public void PrintEmphasized(Direction direction, string ident, string format, params object[] args)
        {
        }

        /// <inheritdoc />
        public void PrintError(string format, params object[] args)
        {
        }

        /// <inheritdoc />
        public void PrintError(Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
        }

        /// <inheritdoc />
        public void PrintError(Direction direction, string ident, string format, params object[] args)
        {
        }

        /// <inheritdoc />
        public void PrintWarning(ReportVerbosity verbosity, string format, params object[] args)
        {
        }

        /// <inheritdoc />
        public void PrintWarning(ReportVerbosity verbosity, Direction direction, IPEndPoint endPoint, string format, params object[] args)
        {
        }

        /// <inheritdoc />
        public void PrintWarning(ReportVerbosity verbosity, Direction direction, string ident, string format, params object[] args)
        {
        }

        /// <inheritdoc />
        public void PrintException(Exception ex, string format, params object[] args)
        {
        }

        /// <inheritdoc />
        public void PrintOscPackets(Direction direction, params OscPacket[] packets)
        {
        }

        /// <inheritdoc />
        public void PrintOscPackets(Direction direction, IPEndPoint endPoint, params OscPacket[] packets)
        {
        }
    }
}