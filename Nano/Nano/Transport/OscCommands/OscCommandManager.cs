﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
using Nano.Transport.Comms;
using Rug.Osc;
using System;
using System.Collections;
using System.Reflection;

namespace Nano.Transport.OSCCommands
{
    public class OscCommandManager
    {
        /// <summary>
        /// Loads the OSC commands from the given provider.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="provider">The provider.</param>
        public static void LoadCommands(OscCommandContext context, ICommandProvider provider)
        {
            Comms.TransportContext transportContext = context.TransportContext;

            provider.AttachTransportContext(context.TransportContext);

            transportContext.Reporter?.PrintDebug("Loading commands from provider {0}", provider.GetType());
            //Loop over all the methods and add OSCCommandAttribute methods.
            foreach (System.Reflection.MethodInfo method in provider.GetType().GetMethods())
            {
                OscCommandAttribute[] customAttributes = (OscCommandAttribute[])method.GetCustomAttributes(typeof(OscCommandAttribute), false);
                foreach (OscCommandAttribute oscAttribute in customAttributes)
                {
                    string address = OscCommandHelper.GetAddressOfMethod(provider.Address, method);
                    oscAttribute.Address = address;

                    //Create a delegate
                    OscMessageEvent del = (OscMessageEvent)Delegate.CreateDelegate(typeof(OscMessageEvent), provider, method);
                    transportContext.OscManager.Attach(address, del);

                    context.Commands.Add(oscAttribute);
                    transportContext.Transmitter.Broadcast(TransportPacketPriority.Critical, new OscMessage(address, oscAttribute.TypeTagString));
                    transportContext.Reporter?.PrintNormal(Direction.Action, "Found command", address);
                }
            }

            //Loop over the Properties and recursively load commands.
            foreach (PropertyInfo property in provider.GetType().GetProperties())
            {
                try
                {
                    var propertyValue = property.GetValue(provider, null);

                    switch (propertyValue)
                    {
                        case ICollection collection:
                            foreach (var x in collection)
                            {
                                if (x is ICommandProvider)
                                {
                                    LoadCommands(context, x as ICommandProvider);
                                }
                            }
                            break;
                        case ICommandProvider commandProvider:
                            LoadCommands(context, commandProvider);
                            break;
                    }
                }
                catch
                {
                }
            }

            //Loop over fields and recursively load commands.
            foreach (FieldInfo field in provider.GetType().GetFields())
            {
                var fieldValue = field.GetValue(provider);

                switch (fieldValue)
                {
                    case ICollection collection:
                        foreach (var x in collection)
                        {
                            if (x is ICommandProvider)
                            {
                                LoadCommands(context, x as ICommandProvider);
                            }
                        }
                        break;
                    case ICommandProvider commandProvider:
                        LoadCommands(context, commandProvider);
                        break;
                }
            }
        }

        /// <summary>
        /// Detaches the commands from the context.
        /// </summary>
        /// <param name="context">The context.</param>
        /// <param name="provider">The provider.</param>
        public static void DetachCommands(OscCommandContext context, ICommandProvider provider)
        {
            Comms.TransportContext transportContext = context.TransportContext;

            transportContext.Reporter.PrintDebug("Detaching commands from provider {0}", provider.GetType());
            
            // Loop over all the methods and add OSCCommandAttribute methods.
            foreach (MethodInfo method in provider.GetType().GetMethods())
            {
                object[] customAttributes = method.GetCustomAttributes(false);
                foreach (object attributes in customAttributes)
                {
                    if (!(attributes is OscCommandAttribute))
                    {
                        continue;
                    }
                    
                    OscCommandAttribute oscAttribute = attributes as OscCommandAttribute;
                    oscAttribute.Address = $"{provider.Address}/{method.Name}";

                    OscMessageEvent del = (OscMessageEvent)Delegate.CreateDelegate(typeof(OscMessageEvent), provider, method);
                    transportContext.OscManager.Detach(oscAttribute.Address, del);
                    transportContext.Reporter.PrintDebug(Direction.Action, "Detached command", oscAttribute.Address);
                }
            }

            //Loop over the Properties and recursively detach commands.
            foreach (PropertyInfo property in provider.GetType().GetProperties())
            {
                var propertyValue = property.GetValue(provider, null);

                switch (propertyValue)
                {
                    case ICollection collection:
                        foreach (var x in collection)
                        {
                            if (x is ICommandProvider)
                            {
                                DetachCommands(context, x as ICommandProvider);
                            }
                        }
                        break;
                    case ICommandProvider commandProvider:
                        DetachCommands(context, commandProvider);
                        break;
                }
            }

            //Loop over fields and recursively detach commands.
            foreach (FieldInfo field in provider.GetType().GetFields())
            {
                var fieldValue = field.GetValue(provider);

                switch (fieldValue)
                {
                    case ICollection collection:
                        foreach (var x in collection)
                        {
                            if (x is ICommandProvider)
                            {
                                DetachCommands(context, x as ICommandProvider);
                            }
                        }
                        break;
                    case ICommandProvider commandProvider:
                        DetachCommands(context, commandProvider);
                        break;
                }
            }
        }
    }
}