﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.
namespace Nano.Transport.Streams
{
    public enum StreamID : int
    {
        AtomPositions = 2,
        AtomTypes = 3,
        Forces = 4,
        Bonds = 5,

        /// <summary>
        /// Used to inform the clients which atoms have been selected by users.
        /// </summary>
        SelectedAtoms = 6,

        SimulationFrame = 7,
        AtomCollision = 8,
        AtomVelocities = 9,

        /// <summary>
        /// Stream for Interaction data structures detailing the type of interaction desired.
        /// </summary>
        /// <remarks>
        /// Use in conjunction with <see cref="InteractionSelectedAtoms"/> to specify groups of atoms to interact with.
        /// </remarks>
        Interaction = 10,

        VRInteraction = 13,
        VRPositions = 14,
        VisibleAtomMap = 15,

        /// <summary>
        /// The atoms that interactions in the Interaction stream correspond to.
        /// TODO is there are way of having structs of varying size in the streams?
        /// </summary>
        /// <remarks> This stream should be packed in the same order as the Interaction stream. A value of -1 is used to indicate the
        /// end of a selection of a particular Interaction.
        /// </remarks>
        /// <example>
        /// 3 interactions, first with 3 atoms, second with a single atom, 3rd with a single atom but unspecified:
        /// 1 4 2 -1 10 -1 -1
        /// ^-1st    ^-2nd ^-3rd
        /// </example>
        InteractionSelectedAtoms = 16,

        /// <summary>
        /// Provides detailed data about the interactions as they are being applied to a system.
        /// </summary>
        /// <remarks>
        /// TODO will make SelectedAtoms obselete.
        /// </remarks>
        InteractionForceInfo = 17, 
        
        /// <summary>
        /// Indicates that the topology has changed. 
        /// </summary>
        TopologyUpdate = 18,
    }
}