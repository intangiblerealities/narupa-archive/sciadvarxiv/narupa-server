﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

namespace Nano.Rainbow
{
    public class PathHelper
    {
        public static string SanitiseFileName(string name)
        {
            return name
                .Trim()
                .Replace("\'", "")
                .Replace("\"", "")
                .Replace("@", "")
                .Replace("}", "")
                .Replace("{", "")
                .Replace("/", "")
                .Replace("\\", "-")
                .Replace("\\", "-")
                .Replace('\t', '-')
                .Replace(' ', '-')
                .Replace("---", "-")
                .Replace("--", "-")
                .ToLowerInvariant();
        }

        public static string SanitiseFilePath(string name)
        {
            return name
                .Trim()
                .Replace("\'", "")
                .Replace("\"", "")
                .Replace("@", "")
                .Replace("}", "")
                .Replace("{", "")
                .Replace("\\", "-")
                .Replace("\\", "-")
                .Replace('\t', '-')
                .Replace(' ', '-')
                .Replace("---", "-")
                .Replace("--", "-")
                .ToLowerInvariant();
        }
    }
}