﻿// Copyright (c) Interactive Scientific LTD. All rights reserved.
// Licensed under the GPL. See License.txt in the project root for license information.

using System;

namespace Nano.Rainbow.v1_0
{
    public enum ReservationResponseType
    {
        Reserved,
        AccessDenied,
        Unavailable
    }

    public class ReservationResponse
    {
        public string Descriptor { get; set; }

        public Uri Host { get; set; }

        public string Name { get; set; }
        public ReservationResponseType Response { get; set; }

        public string SessionToken { get; set; }
    }
}